﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[Serializable]
public struct Damage
{
    public float ammount;
    public DamageType type;
    public Damage(float ammount, DamageType type)
    {
        this.ammount = ammount;
        this.type = type;
    }
    public Damage(int ammount) : this(ammount, DamageType.Absolute)
    {

    }

    public static Damage operator *(Damage a, float b) => new Damage(a.ammount * b, a.type);
    public static Damage operator +(Damage a, float b) => new Damage(a.ammount + b, a.type);
    public static Damage operator -(Damage a, float b) => new Damage(a.ammount - b, a.type);
    public static Damage operator /(Damage a, float b) => new Damage(a.ammount / b, a.type);
}
