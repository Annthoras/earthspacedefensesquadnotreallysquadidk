﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using System.Threading.Tasks;
public class KeyBinding
{
    public KeyCode key;
    public KeyCode mod;
    public int mouseButton = -1;
    public bool onUp;
    public bool onHold;
    public Func<bool> check;
    public static KeyBinding Empty => new KeyBinding();
    public void ConstructCheck()
    {
        if (this == Empty)
            check = () => false;
        else
        {

            if (key != KeyCode.None)
            {
                if (mod != KeyCode.None)
                {
                    if (onUp) check = () => Input.GetKeyDown(mod) && Input.GetKeyUp(key);
                    else if (onHold)
                        check = () => Input.GetKey(mod) && Input.GetKey(key);
                    else check = () => Input.GetKey(mod) && Input.GetKeyDown(key);
                }
                else
                {
                    if (onUp) check = () => Input.GetKeyUp(key);
                    else if (onHold) check = () => Input.GetKey(key);
                    else check = () => Input.GetKeyDown(key);
                }
            }
            else
            {
                if (mod != KeyCode.None)
                {
                    if (onUp) check = () => Input.GetKeyDown(mod) && Input.GetMouseButtonUp(mouseButton);
                    else if (onHold) check = () => Input.GetKey(mod) && Input.GetMouseButton(mouseButton);
                    else check = () => Input.GetKey(mod) && Input.GetMouseButtonDown(mouseButton);
                }
                else
                {
                    if (onUp) check = () => Input.GetMouseButtonUp(mouseButton);
                    else if (onHold) check = () => Input.GetMouseButton(mouseButton);
                    else check = () => Input.GetMouseButtonDown(mouseButton);
                }

            }
        }
    }
    public override string ToString() => (mod != KeyCode.None ? mod.ToString() + " + " : "") + (mouseButton != -1 ? ("Mouse" + mouseButton.ToString()) : key.ToString());
    public static bool operator ==(KeyBinding a, KeyBinding b) => a.key == b.key && a.mod == b.mod && a.mouseButton == b.mouseButton && a.onHold == b.onHold && a.onUp == b.onUp;
    public static bool operator !=(KeyBinding a, KeyBinding b) => !(a == b);
    public override bool Equals(object obj)
    {
        if (obj is KeyBinding)
            return this == (obj as KeyBinding);
        return false;
    }
    public override int GetHashCode()
    {
        return base.GetHashCode();
    }
}
