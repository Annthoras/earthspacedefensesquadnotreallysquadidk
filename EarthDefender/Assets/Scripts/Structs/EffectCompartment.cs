﻿using System;
[Serializable]
public struct EffectCompartment
{
    public AffectableStatistic affectedStat;
    public EffectCalculation calculation;
    public Statistic targetUsedStat;
    public Statistic comparisonStat;
    public float value;
    public bool isAdditive;
    public EffectCompartment(EffectCompartment original)
    {
        this.affectedStat = original.affectedStat;
        this.calculation = original.calculation;
        this.targetUsedStat = original.targetUsedStat;
        this.comparisonStat = original.targetUsedStat;
        this.value = original.value;
        this.isAdditive = original.isAdditive;
    }
}