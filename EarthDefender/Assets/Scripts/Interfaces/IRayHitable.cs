﻿public interface IRayHitable
{
    void OnRayHit();
    void OnRayLeave();
}

