﻿public interface IFactionable
{
    int Faction { get; }
}