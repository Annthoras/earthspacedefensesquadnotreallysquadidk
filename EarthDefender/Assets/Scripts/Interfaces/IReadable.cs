﻿public interface IReadable<T> where T : IReadable<T>
{
    public void Read(string input);
}