﻿using System;
using System.Linq;
using System.Collections;
using UnityEngine;
using Extensions;
using Extensions.Unity;
public class Pickup : MonoBehaviour
{
    public ScriptablePickup _reference;
    public ScriptablePickup Reference
    {
        get => _reference;
        set
        {
            _reference = value;
        }
    }
    private new SpriteRenderer renderer;

    public static Pickup Create()
    {
        Pickup result = GameManager.pickups.FirstOrDefault(x => !x.gameObject.activeSelf);
        if (result == null)
        {
            result = GameObject.Instantiate<Pickup>(Resources.Load<Pickup>("Pickups/Pickup"));
            GameManager.pickups.Add(result);
        }
        result.OnCreate();
        return result;
    }
    private void OnCreate()
    {
        this.renderer = this.GetComponent<SpriteRenderer>();
    }
    public void Init(ScriptablePickup reference, Vector2 origin)
    {
        this.gameObject.SetActive(true);
        this.Reference = reference;
        GameManager.OnFixedUpdate += OnFixedUpdate;
        this.transform.position = origin;
        this.renderer.sprite = reference.sprite;
        this.StartCoroutine(Fall());
    }
    public void OnFixedUpdate()
    {
        float distToDefender = Vector3.Distance(this.transform.position, Defender.main.transform.position);
        if (distToDefender < 1f)
        {
            Reference.effects.ForEach(x =>
            {
                x.compartments.ForEach(c =>
                {
                    if (x.isOffensive)
                        Defender.main.ApplyEffectToWeapon(new Effect(x, c), x.duration);
                    else
                        Defender.main.ApplyEffect(new Effect(x, c));
                });
            });
            GameManager.OnFixedUpdate -= OnFixedUpdate;
            this.gameObject.SetActive(false);
        };
    }
    private IEnumerator Fall()
    {
        bool isFalling = true;
        while (GameManager.IsRunning && isFalling)
        {
            while (!GameManager.IsPaused && this.gameObject.activeSelf)
            {
                if (!Physics.Raycast(this.transform.position, Vector2.down, 1f, 1<<8))
                    this.transform.Translate(Vector2.down * Time.deltaTime, Space.World);
                else
                {
                    isFalling = false;
                    break;
                }
                yield return new WaitForEndOfFrame();
            }
            yield return new WaitForEndOfFrame();
        }
    }
}

