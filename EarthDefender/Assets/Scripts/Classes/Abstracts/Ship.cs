﻿#define Test
#define Debug
#if Debug
//#define Debug_Explicit
#define Debug_Rays
#endif
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Extensions.Unity;
using Extensions;

public abstract class Ship : MonoBehaviour, IFactionable
{
    #region Encapsulated Variables
#if Debug
    [Space]
    [Header("Values")]
    [SerializeField]
#endif
    private float health;
#if Debug
    [SerializeField]
#endif
    private float maxHealth;
#if Debug
    [SerializeField]
#endif
    private float shield;
#if Debug
    [SerializeField]
#endif
    private float maxShield;
#if Debug
    [SerializeField]
#endif
    private float shieldRegen;
#if Debug
    [SerializeField]
#endif
    private float shieldRegenDelay;
#if Debug
    [SerializeField]
#endif
    private float armor;
#if Debug
    [SerializeField]
#endif
    private float maxArmor;
#if Debug
    [SerializeField]
#endif
    private float energy;
#if Debug
    [SerializeField]
#endif
    private float maxEnergy;
#if Debug
    [SerializeField]
#endif
    private float speed;
#if Debug
    [SerializeField]
#endif
    private float turretTurnSpeed;
#if Debug
    [SerializeField]
#endif
    private float fireRate;
#if Debug
    [SerializeField]
#endif
    private float projectileSpeed;
#if Debug
    [SerializeField]
#endif
    private float damageModifier;
#if Debug
    [SerializeField]
#endif
    private float maxAmmo;
#if Debug
    [SerializeField]
#endif
    private ScriptableWeapon currentWeapon;
#if Debug
    [Space]
    [SerializeField]
    private bool isAlive;
#endif
#if Debug
    [Space]
    [SerializeField]
#endif
    private SerializableShip reference;
    #endregion
#if Debug
    [Space]
    [SerializeField]
#endif
    private bool canRegenShield;
#if Debug
    [SerializeField]
#endif
    private float shieldRegenWait;
#if Debug
    [SerializeField]
#endif
    private bool isWaitingForRegen;
#if Debug
    [SerializeField]
#endif
    protected Transform turret;
    public Weapon weapon { get; private set; }
    protected ScriptableWeapon CurrentWeapon
    {
        get => currentWeapon; set
        {
            currentWeapon = value;
            if (!ammoCount.ContainsKey(currentWeapon.usedProjectile))
            {
                ammoCount.Add(currentWeapon.usedProjectile, (int)MaxAmmo);

            }
            weapon.Reference = currentWeapon;
        }
    }
    public SerializableShip Reference
    {
        get => reference;
        protected set
        {
            reference = value;
            OnReferenceChange();
        }
    }

    public int Faction { get; protected set; }
    private Dictionary<Statistic, List<float>> modifiers = new Dictionary<Statistic, List<float>>();
    protected Dictionary<Statistic, Action> onStatChangeEvents = new Dictionary<Statistic, Action>();
    protected List<Effect> activeEffects = new List<Effect>();
    protected Dictionary<ScriptableProjectile, int> ammoCount = new Dictionary<ScriptableProjectile, int>();
    public List<Ability> abilities = new List<Ability>();
    private new SpriteRenderer renderer;
    private bool canMove = true;
    #region Stats
    #region Health
    [Stat(Statistic.Health)]
    public float Health
    {
        get => health;
        private set
        {
            if (value < health)
            {
                canRegenShield = false;
                shieldRegenWait = shieldRegenDelay;
                if (!isWaitingForRegen && value > 0)
                    BeginCoroutine(ShieldRegenWait);
            }
            if (value > maxHealth)
                health = maxHealth;
            else if (value > 0)
                health = value;
            else Die();
            onStatChangeEvents[Statistic.Health]();
        }
    }
    [Stat(Statistic.MaxHealth)]
    public float MaxHealth
    {
        get => maxHealth;
        private set
        {
            if (value > 0)
                maxHealth = value;
            else MaxHealth = 1;
            onStatChangeEvents[Statistic.MaxHealth]();
        }
    }
    #endregion
    #region Shield
    [Stat(Statistic.Shield)]
    public float Shield
    {
        get => shield;
        private set
        {
            if (value < shield)
            {
                canRegenShield = false;
                shieldRegenWait = shieldRegenDelay;
                if (!isWaitingForRegen)
                    BeginCoroutine(ShieldRegenWait);
            }
            if (value > maxShield)
                shield = maxShield;
            else if (value > 0)
                shield = value;
            else
                shield = 0;
            onStatChangeEvents[Statistic.Shield]();
        }
    }
    [Stat(Statistic.MaxShield)]
    public float MaxShield
    {
        get => maxShield;
        private set
        {
            if (value > 0)
                maxShield = value;
            else
                maxShield = 0;
            onStatChangeEvents[Statistic.MaxShield]();
        }
    }
    [Stat(Statistic.ShieldRegen)]
    public float ShieldRegen
    {
        get => shieldRegen;
        private set
        {
            if (value > 0)
                shieldRegen = value;
            else shieldRegen = 0;
            onStatChangeEvents[Statistic.ShieldRegen]();
        }
    }
    [Stat(Statistic.ShieldRegenDelay)]
    public float ShieldRegenDelay
    {
        get => shieldRegenDelay;
        private set
        {
            if (value > 0)
                shieldRegenDelay = value;
            else shieldRegenDelay = 0;
            onStatChangeEvents[Statistic.ShieldRegenDelay]();
        }
    }
    #endregion
    #region Armor
    [Stat(Statistic.Armor)]
    public float Armor
    {
        get => armor;
        private set
        {
            if (value < armor)
            {
                canRegenShield = false;
                shieldRegenWait = shieldRegenDelay;
                if (!isWaitingForRegen)
                    BeginCoroutine(ShieldRegenWait);
            }
            if (value > maxArmor)
                armor = maxArmor;
            else if (value > 0)
                armor = value;
            else armor = 0;
            onStatChangeEvents[Statistic.Armor]();
        }
    }
    [Stat(Statistic.MaxArmor)]
    public float MaxArmor
    {
        get => maxArmor; private set
        {
            if (value > 0)
                maxArmor = value;
            else maxArmor = 0;
            onStatChangeEvents[Statistic.MaxArmor]();
        }
    }
    #endregion
    #region Energy
    [Stat(Statistic.Energy)]
    public float Energy
    {
        get => energy;
        private set
        {
            if (value > maxEnergy)
                energy = maxEnergy;
            else if (value > 0)
                energy = value;
            else energy = 0;
            onStatChangeEvents[Statistic.Energy]();
        }
    }
    [Stat(Statistic.MaxEnergy)]
    public float MaxEnergy
    {
        get => maxEnergy; private set
        {
            if (value > 0)
                maxEnergy = value;
            else maxEnergy = 0;
            onStatChangeEvents[Statistic.MaxEnergy]();
        }
    }
    #endregion
    #region Speed
    [Stat(Statistic.Speed)]
    public float Speed
    {
        get => speed;
        private set
        {
            speed = value;
            if (speed < 0)
                speed = 0;
            onStatChangeEvents[Statistic.Speed]();
        }
    }
    [Stat(Statistic.TurretTurnSpeed)]
    public float TurretTurnSpeed
    {
        get => turretTurnSpeed;
        private set
        {
            turretTurnSpeed = value;
            if (turretTurnSpeed < 0)
                turretTurnSpeed = 0;
            onStatChangeEvents[Statistic.TurretTurnSpeed]();
        }
    }
    #endregion
    #region Firerate
    [Stat(Statistic.Firerate)]
    public float FireRate
    {
        get => fireRate; private set
        {
            if (value > 0)
                fireRate = value;
            else fireRate = 0;
            onStatChangeEvents[Statistic.Firerate]();
        }
    }
    #endregion
    #region ProjectileSpeed
    [Stat(Statistic.ProjectileSpeed)]
    public float ProjectileSpeed
    {
        get => projectileSpeed; private set
        {
            if (value > 0.1f)
                projectileSpeed = value;
            else projectileSpeed = 0.1f;
            onStatChangeEvents[Statistic.ProjectileSpeed]();
        }
    }
    #endregion
    #region Damage
    [Stat(Statistic.Damage)]
    public float DamageModifier
    {
        get => damageModifier; private set
        {
            if (value > 0.1f)
                damageModifier = value;
            else damageModifier = 0.1f;
            onStatChangeEvents[Statistic.Damage]();
        }
    }
    #endregion
    #region Ammo
    [Stat(Statistic.MaxAmmo)]
    public float MaxAmmo
    {
        get
        {
            if (CurrentWeapon.infiniteAmmo)
                return -1;
            return BaseAmmo + modifiers[Statistic.MaxAmmo].Sum();
        }
    }
    [Stat(Statistic.BaseAmmo)]
    public float BaseAmmo
    {

        get
        {
            return this.reference.maxProjectilesAmmounts[reference.maxProjectiles.IndexOf(currentWeapon.usedProjectile.name)];
        }
    }
    [Stat(Statistic.Ammo)]
    public float Ammo
    {
        get => ammoCount[CurrentWeapon.usedProjectile];
        set
        {
            if (value > MaxAmmo)
                ammoCount[CurrentWeapon.usedProjectile] = (int)MaxAmmo;
            else
                ammoCount[CurrentWeapon.usedProjectile] = (int)value;
        }
    }
    #endregion
    #endregion

    #region Events
    public event Action<Statistic, int> OnModifierRemove = delegate { };
    public event Action<Effect> OnEffectApply = delegate { };
    public event Action<Effect, float> OnWeaponEffectApply = delegate { };

    #endregion

    private new Collider collider;

    public float GetStat(Statistic stat)
    {
#if Debug_Explicit
        Debug.Log("GetStat() from " + this.name);
        Debug.Log("Enum: " + stat);
        PropertyInfo askedForStat = GetStatProperty(stat);
        Debug.Log("Stat: " + askedForStat.Name);
        float value;
        if (askedForStat.Name.Contains("Base"))
        {
            value = (float)askedForStat.GetValue(this.reference);
            Debug.Log(askedForStat.GetValue(this.reference).GetType());
        }
        else
            value = (float)askedForStat.GetValue(this);
        Debug.Log(askedForStat.Name + " Value: " + value.GetType());
        return value;
#else
        PropertyInfo info = GetStatProperty(stat);
        if (stat.ToString().Contains("Base") && stat != Statistic.BaseAmmo)
            return (float)info.GetValue(this.reference);
        else return (float)info.GetValue(this);
#endif

    }
    public void SetStat(Statistic stat, float value)
    {
#if Debug_Explicit
        Debug.Log("SetStat()");
        Debug.Log("Enum:" + stat);
        PropertyInfo askedForStat = GetStatProperty(stat);
        Debug.Log("Stat:" + askedForStat.Name);
        object currentValue = askedForStat.GetValue(this);
        Debug.Log("CurrentValue:" + currentValue);
        askedForStat.SetValue(this, value);
        Debug.Log("New Value:" + value);
#else
        GetStatProperty(stat).SetValue(this, value);
#endif
    }
    private PropertyInfo GetStatProperty(Statistic stat)
    {

        Type requiredType;
        if (stat == Statistic.BaseAmmo)
            requiredType = typeof(Ship);
        else
            requiredType = stat.ToString().Contains("Base") ? typeof(ScriptableShip) : typeof(Ship);

        return requiredType.GetProperties().First(x => x.GetCustomAttribute<Stat>() != null && x.GetCustomAttribute<Stat>().Statistic == stat);
    }

    private void Recalculate(Statistic stat)
    {
        PropertyInfo info = GetStatProperty(stat);
        Statistic baseStat;
        PropertyInfo baseInfo;
        if (Enum.TryParse<Statistic>("Base" + stat.ToString().Replace("Max", ""), out baseStat))
        {
            baseInfo = GetStatProperty(baseStat);
            info.SetValue(this, (float)(baseInfo.GetValue(this.reference)) + modifiers[stat].Sum());
        }
    }
    public float GetModifier(Statistic stat) => modifiers[stat].Sum();
    public int AddModifier(Statistic stat, float value)
    {
        modifiers[stat].Add(value);
        Recalculate(stat);
        return modifiers[stat].Count - 1;
    }
    public void ChangeModifier(Statistic stat, int index, float newVal)
    {
        modifiers[stat][index] = newVal;
        Recalculate(stat);
    }
    public void RemoveModifier(Statistic stat, int index)
    {
        try
        {
            modifiers[stat].RemoveAt(index);
            Recalculate(stat);
            OnModifierRemove(stat, index);

        }
        catch (Exception e) { }
    }
    public void OnStatChangeBind(Statistic stat, Action act) => onStatChangeEvents[stat] += act;
    public void OnStatChangeUnbind(Statistic stat, Action act) => onStatChangeEvents[stat] -= act;

    public void GetHit(Projectile projectile)
    {
        GetDamage(projectile.damage);

        projectile.Effects.ForEach(x =>
        {
            ApplyEffect(x);
        });
    }
    public void ApplyEffect(Effect effect)
    {
        if (effect.reference.isUnique)
        {
            activeEffects.Where(x => (x.reference.isUnique && x.reference == effect.reference) || effect.reference.uniques.Any(u => u == x.reference)).ForEach(x => x.Cancel());
            activeEffects = activeEffects.Where(x => !x.reference.isUnique && x.reference != effect.reference && !effect.reference.uniques.Any(u => u == x.reference)).ToList();
        }
        activeEffects.Add(effect);
        effect.Apply(this, () => activeEffects.Remove(effect));
        if (effect.reference.duration > 0)
            OnEffectApply(effect);
    }
    public void ApplyEffectToWeapon(Effect effect, float duration)
    {
        this.weapon.AddEffect(effect, duration);
        OnWeaponEffectApply(effect, duration);

    }
    public void GetDamage(Damage dmg)
    {

        float remainingDamage = 0;
        switch (dmg.type)
        {
            case DamageType.Absolute:
                if (shield > 0)
                    Shield -= dmg.ammount;
                else
                {
                    float healthDamage = dmg.ammount;
                    if (armor > 0)
                        if (armor >= dmg.ammount / 3)
                        {
                            Armor -= dmg.ammount / 3;
                            healthDamage = (dmg.ammount / 3) * 2;
                        }
                        else
                        {
                            healthDamage = dmg.ammount - (dmg.ammount / 3 - armor);
                            Armor = 0;
                        }
                    Health -= healthDamage;
                }
                break;
            case DamageType.Piercing:
                if (shield > 0)
                    Shield -= dmg.ammount;
                else
                {
                    float healthDamage = dmg.ammount;
                    if (armor > 0)
                        if (armor >= dmg.ammount / 4)
                        {
                            Armor -= dmg.ammount / 4;
                            healthDamage = (dmg.ammount / 4) * 3;
                        }
                        else
                        {
                            healthDamage = dmg.ammount - (dmg.ammount / 4 - armor);
                            Armor = 0;
                        }
                    Health -= healthDamage;
                }
                break;
            case DamageType.Impact:
                canMove = false;
                BeginCoroutine(ConcussionWait);
                if (shield > 0)
                    Shield -= dmg.ammount;
                else
                {
                    float healthDamage = dmg.ammount;
                    if (armor > 0)
                        if (armor >= dmg.ammount / 2)
                        {
                            Armor -= dmg.ammount / 2;
                            healthDamage = (dmg.ammount / 2);
                        }
                        else
                        {
                            healthDamage = dmg.ammount - (dmg.ammount / 2 - armor);
                            Armor = 0;
                        }
                    Health -= healthDamage;
                }
                break;
            case DamageType.Fire:
                //To Do: Fire Effect
                if (shield > 0)
                    Shield -= dmg.ammount;
                else
                {
                    float healthDamage = dmg.ammount;
                    if (armor > 0)
                        if (armor >= dmg.ammount / 3)
                        {
                            Armor -= dmg.ammount / 3;
                            healthDamage = (dmg.ammount / 3) * 2;
                        }
                        else
                        {
                            healthDamage = dmg.ammount - (dmg.ammount / 3 - armor);
                            Armor = 0;
                        }
                    Health -= healthDamage;
                }
                break;
            case DamageType.Explosive:
                //To Do: Explosive Effect
                if (shield > 0)
                    Shield -= dmg.ammount;
                else
                {
                    float healthDamage = dmg.ammount;
                    if (armor > 0)
                        if (armor >= dmg.ammount / 2)
                        {
                            Armor -= dmg.ammount / 2;
                            healthDamage = (dmg.ammount / 2);
                        }
                        else
                        {
                            healthDamage = dmg.ammount - (dmg.ammount / 2 - armor);
                            Armor = 0;
                        }
                    Health -= healthDamage;
                }
                break;
            case DamageType.Frost:
                //To Do: Frost Effect
                if (shield > 0)
                    Shield -= dmg.ammount;
                else
                {
                    float healthDamage = dmg.ammount;
                    if (armor > 0)
                        if (armor >= dmg.ammount / 3)
                        {
                            Armor -= dmg.ammount / 3;
                            healthDamage = (dmg.ammount / 3) * 2;
                        }
                        else
                        {
                            healthDamage = dmg.ammount - (dmg.ammount / 3 - armor);
                            Armor = 0;
                        }
                    Health -= healthDamage;
                }
                break;
            case DamageType.Electric:
                //To Do: Electric Effect
                if (shield > 0)
                    remainingDamage = dmg.ammount - shield;
                if (remainingDamage > 0)
                {
                    float healthDamage = dmg.ammount;
                    if (armor > 0)
                        if (armor >= dmg.ammount / 3)
                        {
                            Armor -= dmg.ammount / 3;
                            healthDamage = (dmg.ammount / 3) * 2;
                        }
                        else
                        {
                            healthDamage = dmg.ammount - (dmg.ammount / 3 - armor);
                            Armor = 0;
                        }
                    Health -= healthDamage;
                }
                break;
            case DamageType.Corrosive:
                //To Do: Corrosive Effect
                if (shield > 0)
                    Shield -= dmg.ammount;
                else
                {
                    float healthDamage = dmg.ammount;
                    if (armor > 0)
                        if (armor >= (dmg.ammount / 3) * 2)
                        {
                            Armor -= (dmg.ammount / 3) * 2;
                            healthDamage = (dmg.ammount / 3);
                        }
                        else
                        {
                            healthDamage = dmg.ammount - ((dmg.ammount / 3) * 2 - armor);
                            Armor = 0;
                        }
                    Health -= healthDamage;
                }
                break;
            case DamageType.Magnetic:
                //To Do: Magnetic Effect
                if (shield > 0)
                    remainingDamage = dmg.ammount - shield;
                if (remainingDamage > 0)
                {
                    float healthDamage = dmg.ammount;
                    if (armor > 0)
                        if (armor >= dmg.ammount)
                            Armor -= dmg.ammount;
                        else
                        {
                            healthDamage = dmg.ammount - (dmg.ammount - armor);
                            Armor = 0;
                        }
                    Health -= healthDamage;
                }
                break;
        }
    }
    public virtual void Init(SerializableShip reference)
    {
        this.Reference = reference;
        canMove = true;
    }
    protected virtual void OnReferenceChange()
    {
        IEnumerable<Statistic> stats = Enum.GetValues(typeof(Statistic)).Cast<Statistic>();
        foreach (var s in stats)
        {
            modifiers[s] = new List<float>();
            onStatChangeEvents[s] = delegate { };
        }
        this.maxHealth = reference.baseHealth;
        this.health = maxHealth;
        this.maxArmor = reference.baseArmor;
        this.armor = maxArmor;
        this.maxShield = reference.baseShield;
        this.shield = maxShield;
        this.speed = reference.baseSpeed;
        this.shieldRegen = reference.baseShieldRegen;
        this.shieldRegenDelay = reference.baseShieldRegenDelay;
        this.maxEnergy = reference.baseEnergy;
        this.energy = maxEnergy;
        this.fireRate = reference.baseFireRate;
        this.projectileSpeed = 1;
        this.damageModifier = 1;
        this.turretTurnSpeed = reference.turretTurnSpeed;
        this.CurrentWeapon = Resources.Load<ScriptableWeapon>(FileManager.ScriptableWeapons + reference.baseWeaponName);
        this.renderer.color = reference.color;
        abilities = new List<Ability>(reference.abilities.ForEach(x => { return new Ability(this, Resources.Load<ScriptableAbilitiy>(FileManager.ScriptableAbilities + x)); }));
        this.activeEffects = new List<Effect>();
    }
    protected virtual void OnCreate()
    {
        this.collider = this.GetComponent<Collider>();
        weapon = this.transform.GetComponentInChildren<Weapon>();
        weapon.owner = this;
        weapon.Init();
        turret = this.transform.Find("TurretPivot");
        this.renderer = this.GetComponentInChildren<SpriteRenderer>();
    }
    /*
    private void RecheckMaxAmmo()
    {
        // what the fuck was this for?
        //you're setting the current ammo ammount to max ammo every time Max ammo is rechecked 
        //you absolute melt
        List<ScriptableProjectile> proj = ammoCount.Keys.ForEach(x => x).ToList();
        for (int i = 0; i < proj.Count; i++)
        {
            ammoCount[proj[i]] = reference.maxProjectilesAmmounts[reference.maxProjectiles.IndexOf(proj[i])] + (int)modifiers[Statistic.MaxAmmo].Sum();
        }
    }
    */
    public abstract void LookAt(Vector3 target);

    protected virtual void Fire()
    {
        weapon.Fire();
    }

    protected virtual void Die()
    {
        StopAllCoroutines();
    }

    public bool Move(Vector2Int movementDirection)
    {
        Vector2 movementRaycastOrigin = new Vector2(this.transform.position.x + (movementDirection.x * (this.collider.bounds.size.x / 2)), this.transform.position.y + (movementDirection.y * (this.collider.bounds.size.y / 2)));
#if Debug_Rays
        Debug.DrawRay(movementRaycastOrigin, movementDirection.ToVector2().normalized * speed * Time.deltaTime, Color.red);
#endif
        if (canMove)
        {

            RaycastHit hit;
            if (!Physics.Raycast(movementRaycastOrigin, movementDirection.ToVector2().normalized, out hit, speed * Time.deltaTime))
            {
                this.transform.Translate(movementDirection.ToVector2().normalized * Time.deltaTime * speed, Space.World);
                return true;
            }
            IFactionable factionable = hit.collider.GetComponent<IFactionable>();
            if (factionable != null)
            {
                if (factionable is Projectile || factionable.Faction == this.Faction)
                {
                    this.transform.Translate(movementDirection.ToVector2().normalized * Time.deltaTime * speed, Space.World);
                    return true;
                }
                return false;
            }
            return false;
        }
        return false;

    }
    public void BeginCoroutine<T>(Func<T, IEnumerator> routine, T a)
    {
        if (this.gameObject.activeSelf)
            StartCoroutine(routine(a));
    }
    public void BeginCoroutine<T1, T2>(Func<T1, T2, IEnumerator> routine, T1 a, T2 b)
    {
        if (this.gameObject.activeSelf)
            StartCoroutine(routine(a, b));
    }
    public void BeginCoroutine(Func<IEnumerator> routine)
    {
        if (this.gameObject.activeSelf)
            StartCoroutine(routine());
    }

    protected IEnumerator ShieldRegeneration()
    {
        while (GameManager.IsRunning && (shield < maxShield && canRegenShield))
        {
            while (!GameManager.IsPaused && (shield < MaxShield && canRegenShield))
            {

                Shield += shieldRegen;
                yield return new WaitForSeconds(1);
            }
            yield return null;
        }

    }
    protected IEnumerator ShieldRegenWait()
    {
        isWaitingForRegen = true;
        while (GameManager.IsRunning && (!canRegenShield && shieldRegenWait > 0))
        {
            while (!GameManager.IsPaused && (!canRegenShield && shieldRegenWait > 0))
            {
                shieldRegenWait -= Time.fixedDeltaTime;
                yield return new WaitForFixedUpdate();
            }
            yield return null;
        }
        canRegenShield = true;
        isWaitingForRegen = false;
        BeginCoroutine(ShieldRegeneration);
    }
    private IEnumerator ConcussionWait()
    {
        float secsLeft = 0.3f;
        while (GameManager.IsRunning && secsLeft > 0)
        {
            while (!GameManager.IsPaused && secsLeft > 0)
            {
                secsLeft -= Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }
            yield return null;
        }
        canMove = true;
    }

}

