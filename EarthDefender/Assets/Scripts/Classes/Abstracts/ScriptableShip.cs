﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Extensions;
public abstract class ScriptableShip : ScriptableObject
{
    public float baseHealth;
    public float baseShield;
    public float baseShieldRegen;
    public float baseShieldRegenDelay;
    public float baseArmor;
    public float baseSpeed;
    public float baseEnergy;
    public float baseFireRate = 1;
    public float turretTurnSpeed = 1;
    public Sprite sprite;
    public Color color;
    public List<ScriptableAbilitiy> abilities = new List<ScriptableAbilitiy>();
    public List<ScriptableProjectile> maxProjectiles = new List<ScriptableProjectile>();
    public List<int> maxProjectilesAmmounts = new List<int>();
    public abstract int Faction { get; }
    [Stat(Statistic.BaseHealth)]
    public float BaseHealth { get => baseHealth; }
    [Stat(Statistic.BaseShield)]
    public float BaseShield { get => baseShield; }
    [Stat(Statistic.BaseShieldRegen)]
    public float BaseShieldRegen { get => baseShieldRegen; }
    [Stat(Statistic.BaseShieldRegenDelay)]
    public float BaseShieldRegenDelay { get => baseShieldRegenDelay; }
    [Stat(Statistic.BaseArmor)]
    public float BaseArmor { get => baseArmor; }
    [Stat(Statistic.BaseSpeed)]
    public float BaseSpeed { get => baseSpeed; }
    [Stat(Statistic.BaseEnergy)]
    public float BaseEnergy { get => baseEnergy; }
    [Stat(Statistic.BaseFirerate)]
    public float BaseFirerate { get => baseFireRate; }
    [Stat(Statistic.TurretTurnSpeed)]
    public float TurretTurnSpeed { get => turretTurnSpeed; }

    public ScriptableWeapon baseWeapon;



    public virtual void FromSerialized(SerializableShip s)
    {
        this.baseHealth = s.baseHealth;
        this.baseShield = s.baseShield;
        this.baseShieldRegen = s.baseShieldRegen;
        this.baseShieldRegenDelay = s.baseShieldRegenDelay;
        this.baseArmor = s.baseArmor;
        this.baseSpeed = s.baseSpeed;
        this.baseEnergy = s.baseEnergy;
        this.baseFireRate = s.baseFireRate;
        this.turretTurnSpeed = s.turretTurnSpeed;
        this.sprite = Resources.Load<Sprite>(((s is SerializableInvader) ? FileManager.Invaders : FileManager.Defenders) + s.sprite);
        this.color = s.color;
        this.abilities = s.abilities.ForEach(x => { return Resources.Load<ScriptableAbilitiy>(FileManager.ScriptableAbilities); }).ToList();
        this.maxProjectiles = s.maxProjectiles.ForEach(x => { return Resources.Load<ScriptableProjectile>(FileManager.ScriptableProjectiles + x); }).ToList();
        this.maxProjectilesAmmounts = new List<int>(s.maxProjectilesAmmounts);
        this.baseWeapon = Resources.Load<ScriptableWeapon>(FileManager.ScriptableWeapons +s.baseWeaponName);
    }


    public float GetStat(Statistic stat)
    {
        switch (stat)
        {
            case Statistic.Health:
            case Statistic.BaseHealth:
            case Statistic.MaxHealth:
                return baseHealth;
            case Statistic.Speed:
            case Statistic.BaseSpeed:
                return baseSpeed;
            case Statistic.Energy:
            case Statistic.BaseEnergy:
            case Statistic.MaxEnergy:
                return baseEnergy;
            case Statistic.Shield:
            case Statistic.BaseShield:
            case Statistic.MaxShield:
                return baseShield;
            case Statistic.ShieldRegen:
            case Statistic.BaseShieldRegen:
                return baseShieldRegen;
            case Statistic.ShieldRegenDelay:
            case Statistic.BaseShieldRegenDelay:
                return baseShieldRegenDelay;
            case Statistic.Armor:
            case Statistic.BaseArmor:
            case Statistic.MaxArmor:
                return baseArmor;
            case Statistic.Firerate:
            case Statistic.BaseFirerate:
                return baseFireRate;
            case Statistic.Ammo:
            case Statistic.BaseAmmo:
            case Statistic.MaxAmmo:
                return 0;
            case Statistic.ProjectileSpeed:
                return 0;
            case Statistic.Damage:
                return 1;
            case Statistic.TurretTurnSpeed:
                return turretTurnSpeed;
            default:
                return 0;
        }
    }

}

