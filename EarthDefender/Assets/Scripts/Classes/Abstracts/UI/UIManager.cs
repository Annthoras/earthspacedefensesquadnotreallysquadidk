﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using TMPro;
using Extensions;
[RequireComponent(typeof(Canvas))]
public abstract class UIManager : MonoBehaviour
{
    private static IRayHitable _rayTarget;
    public static UIManager current { get; protected set; }
    protected static Camera currentCam;
    protected Transform optionsUI;
    public static Ray MouseRay { get => currentCam.ScreenPointToRay(Input.mousePosition); }
    public static IRayHitable RayTarget
    {
        get => _rayTarget;
        set
        {
            IRayHitable previous = _rayTarget;
            if (previous != null)
                _rayTarget.OnRayLeave();
            _rayTarget = value;
            if (_rayTarget != null)
                _rayTarget.OnRayHit();
        }
    }
    public void ChangeKeybind(GameObject caller)
    {
        caller.GetComponentInChildren<TextMeshProUGUI>().text = "Awaiting Input...";
        StartCoroutine(ChangeKeyBind(caller));
    }

    private IEnumerator ChangeKeyBind(GameObject caller)
    {
        string name = caller.transform.parent.name;
        int index = caller.name.Replace("Bind", "")[0] - 49;
        bool isBinding = true;
        KeyBinding newBind = new KeyBinding();
        IEnumerable<KeyCode> viableKeys = Enum.GetValues(typeof(KeyCode)).Cast<KeyCode>().Where(x => (int)x > 27 && (int)x < 282);
        while (GameManager.IsRunning && isBinding)
        {
            if (!Input.anyKey || !Input.anyKeyDown)
            {
                newBind.mod = Input.GetKey(KeyCode.LeftShift) ? KeyCode.LeftShift : (Input.GetKey(KeyCode.LeftControl) ? KeyCode.LeftControl : (Input.GetKey(KeyCode.LeftAlt) ? KeyCode.LeftAlt : KeyCode.None));
                newBind.mouseButton = Input.GetMouseButton(0) ? 0 : (Input.GetMouseButton(1) ? 1 : (Input.GetMouseButton(2) ? 2 : (Input.GetMouseButton(3) ? 3 : Input.GetMouseButton(4) ? 4 : -1)));
                if (newBind.mouseButton != -1)
                    isBinding = false;
                else
                    foreach (var x in viableKeys)
                    {
                        if (Input.GetKey(x))
                        {
                            newBind.key = x;
                            isBinding = false;
                            break;
                        }
                    }
            }
            yield return null;
        }
        if (name == "Left" || name == "Right" || name == "Fire")
            newBind.onHold = true;
        newBind.ConstructCheck();
        caller.GetComponentInChildren<TextMeshProUGUI>().text = newBind.ToString();
        InputManager.ReplaceBinding(name.ToLower(), index == 0 ? InputManager.bindings[name.ToLower()].Item1 : InputManager.bindings[name.ToLower()].Item2, newBind);
    }
    protected virtual void Awake()
    {
        if (current != null && current != this)
            Destroy(this);
        current = this;
    }
    protected virtual void Start()
    {
        currentCam = Camera.main;
        optionsUI = this.transform.Find("OptionsUI");
        optionsUI.Find("Bindings").GetComponentsInChildren<Transform>().Where(x => x != optionsUI.Find("Bindings") && x.parent == optionsUI.Find("Bindings")).ForEach(x =>
        {
            Tuple<KeyBinding, KeyBinding> binds = InputManager.bindings[x.name.ToLower()];
            x.Find("Bind1").Find("Text").GetComponent<TextMeshProUGUI>().text = binds.Item1.ToString();
            x.Find("Bind2").Find("Text").GetComponent<TextMeshProUGUI>().text = binds.Item2.ToString();


        });
    }
    protected virtual void Update()
    {
        RaycastHit hit;
        Physics.Raycast(MouseRay, out hit);
        if (hit.collider != null)
            RayTarget = hit.collider.GetComponent<IRayHitable>();
        else RayTarget = null;
    }

    public abstract void OptionsMenu();
    protected virtual void ExitOptions()
    {
        //if(optionsChanged)
        //  ShowOptionsChangedExitDialogue();
    }
    //ShowOptionsChangedExitDilalogue()
    //{
    //}
    protected virtual void OnDestroy()
    {
        current = null;
    }
    public void Quit()
    {
#if UNITY_EDITOR
        EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }

}

