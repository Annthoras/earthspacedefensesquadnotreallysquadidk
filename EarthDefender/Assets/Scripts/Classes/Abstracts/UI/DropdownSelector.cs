using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Extensions;
using TMPro;
public class DropdownSelector : MonoBehaviour
{
    private TMP_Dropdown _dropdown;

    private List<string> _options = new List<string>();
    public List<string> Options
    {
        get => _options;
        protected set
        {
            _options = value;
            _dropdown.options = new List<TMP_Dropdown.OptionData>(_options.ForEach(x => { return new TMP_Dropdown.OptionData(x); }));
        }
    }

    private int _currentIndex;
    public int CurrentIndex
    {
        get => _currentIndex;
        protected set
        {
            _currentIndex = value;
            if (_currentIndex >= _options.Count)
                _currentIndex = 0;
            else if (_currentIndex < 0)
                _currentIndex = _options.Count - 1;
            _dropdown.value = _currentIndex;
            onValueChanged();
        }
    }

    protected Action onValueChanged;
    protected virtual void Awake()
    {
        _dropdown = this.GetComponentInChildren<TMP_Dropdown>();
        _dropdown.onValueChanged.AddListener(OnValueChanged);
        onValueChanged = delegate { };
    }
    private void OnValueChanged(int i){_currentIndex = i; onValueChanged(); }
    public virtual void Next() => CurrentIndex++;

    public virtual void Previous() => CurrentIndex--;

    public void OnValueChangeBind(Action a) => onValueChanged += a;
    public void OnValueChangeUnBind(Action a) => onValueChanged -= a;

}
