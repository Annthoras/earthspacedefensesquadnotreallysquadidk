﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public class Stat : Attribute
{
    protected Statistic stat;
    public Stat(Statistic stat) => this.stat = stat;

    public Statistic Statistic { get => stat; set => stat = value; }
}

