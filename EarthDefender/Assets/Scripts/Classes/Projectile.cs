﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Extensions;
using Extensions.Unity;
public class Projectile : MonoBehaviour, IFactionable
{
    public ScriptableProjectile reference;
    private bool isExistant;
    private new SpriteRenderer renderer;
    public Weapon Owner { get; private set; }
    public Damage damage;
    private float flightSpeedModifier;
    public Effect[] Effects { get; private set; }
    //Yoink Effects from weapon
    public int Faction { get; private set; }
    private Vector2[] collisionRayDirections = new Vector2[]
    {
        new Vector2(0,1), new Vector2(0.3f,0.7f).normalized, new Vector2(-0.3f,-0.7f).normalized
    };
    private static Projectile prefab
    {
        get => Resources.Load<Projectile>("Projectiles/Projectile");
    }
    public static Projectile Create()
    {

        Projectile result = GameManager.projectiles.FirstOrDefault(x => !x.gameObject.activeSelf);
        if (result == null)
        {
            result = GameObject.Instantiate(prefab);
            result.OnCreate();
            GameManager.projectiles.Add(result);
        }
        return result;
    }
    public void OnCreate()
    {
        renderer = this.GetComponent<SpriteRenderer>();

    }

    public void Init(ScriptableProjectile reference, Weapon owner)
    {
        this.gameObject.SetActive(true);
        this.reference = reference;
        this.Owner = owner;
        this.damage = reference.damage * owner.owner.DamageModifier;
        this.flightSpeedModifier = owner.Reference.projectileFlightSpeed * owner.owner.ProjectileSpeed;
        List<Effect> effects = new List<Effect>();
        reference.extraEffects.ForEach(x => effects.AddRange(x.compartments.ForEach(c => { return new Effect(x, c, owner.owner.DamageModifier); })));
        effects.AddRange(owner.extraEffects.Keys);
        this.Effects = effects.ToArray();
        this.Faction = owner.owner.Faction;
        renderer.sprite = reference.sprite;
        if (this.reference.keepColor)
            renderer.color = this.reference.color;
        else renderer.color = owner.Renderer.color;
        isExistant = true;
        StartCoroutine(Move());
        StartCoroutine(AutoRemove());
    }

    private IEnumerator Move()
    {
        while (GameManager.IsRunning && isExistant)
        {
            while (!GameManager.IsPaused)
            {
                this.transform.Translate(this.transform.up * reference.flightSpeed * flightSpeedModifier * Time.deltaTime, Space.World);
                collisionRayDirections.ForEach(x =>
                {
                    RaycastHit hit;
                    if (Physics.Raycast(this.transform.position.ToVector2Y() + new Vector2(0, this.transform.localScale.y / 2), x, out hit))
                    {
                        if (hit.distance <= reference.flightSpeed * flightSpeedModifier * Time.deltaTime)
                        {
                            IFactionable factionable = hit.collider.GetComponent<IFactionable>();
                            if (factionable != null)
                            {
                                if (factionable.Faction != this.Faction)
                                {
                                    Ship ship = factionable as Ship;
                                    if (ship != null)
                                        ship.GetHit(this);
                                    else (factionable as Projectile).OnHit();
                                    OnHit();
                                }
                            }
                            else OnHit();
                        }
                    }
                });
                yield return null;
            }
            yield return null;
        }
    }
    private IEnumerator AutoRemove()
    {
        float secsLeft = 30;
        while (GameManager.IsRunning && isExistant && secsLeft > 0)
        {
            while (!GameManager.IsPaused && isExistant && secsLeft > 0)
            {
                secsLeft -= Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }
            yield return null;
        }
        this.gameObject.SetActive(false);
    }

    private void OnHit()
    {
        isExistant = false;
        if (reference.damage.type == DamageType.Explosive)
        {
            RaycastHit[] hits = Physics.SphereCastAll(this.transform.position, reference.explosionRadius, this.transform.position);
            hits.ForEach(x =>
            {
                IFactionable factionable = x.collider.GetComponent<IFactionable>();
                if (factionable != null)
                {
                    Ship s = factionable as Ship;
                    Projectile p = factionable as Projectile;
                    if (s != null)
                        s.GetHit(this);
                    else if (p != null && p.isExistant)
                        p.OnHit();
                }
            });
        }
        //Spawn Effects
        this.gameObject.SetActive(false);
    }

}
