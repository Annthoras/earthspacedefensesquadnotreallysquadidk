﻿//#define Debug

using System;
using System.Collections;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Threading.Tasks;
using UnityEngine;
[Serializable]
public class Effect
{
    public ScriptableEffect reference;
    private Statistic usableAffected;
    public int TicksLeft { get => ticksLeft; private set => ticksLeft = value; }
    public float SecondsLeft { get => secondsLeft; private set => secondsLeft = value; }
#if Debug
    [SerializeField]
#endif
    private int ticksLeft;
#if Debug
    [SerializeField]
#endif
    private float secondsLeft;
#if Debug
    [SerializeField]
#endif
    private Statistic affected;
#if Debug
    [SerializeField]
#endif
    private EffectCalculation calc;
#if Debug
    [SerializeField]
#endif
    private Statistic targetUsed;
#if Debug
    [SerializeField]
#endif
    private Statistic comparison;
#if Debug
    [SerializeField]
#endif
    private bool isOffensive;
#if Debug
    [SerializeField]
#endif
    private bool isAddable;
#if Debug
    [SerializeField]
#endif
    private bool isAdditive;
#if Debug
    public string targetName;
    public float tickDelay;
    [SerializeField]
#endif
    private float value;
    public float TickDelay { get => reference.duration / reference.ticks; }
    private Func<Ship, float> calculation;
    Ship target;
    private int referenceIndex;
    private bool _continue;
    private bool removedModifier;
    private event Action onFinished = delegate { };

    public Effect(ScriptableEffect reference, EffectCompartment compartment)
    {
        this.reference = reference;
        TicksLeft = this.reference.ticks;
        this.SecondsLeft = this.reference.duration;
        usableAffected = (Statistic)Enum.Parse(typeof(Statistic), compartment.affectedStat.ToString());
        ticksLeft = TicksLeft;
        affected = usableAffected;
        calc = compartment.calculation;
        targetUsed = compartment.targetUsedStat;
        comparison = compartment.comparisonStat;
        isOffensive = this.reference.isOffensive;
        isAddable = affected != Statistic.Health && affected != Statistic.Armor && affected != Statistic.Shield && affected != Statistic.Ammo && affected != Statistic.Energy;
        this.isAdditive = compartment.isAdditive;
        this.value = compartment.value;
        CalculateEffect();
    }
    public Effect(ScriptableEffect reference, EffectCompartment compartment, float modifier) : this(reference, compartment)
    {
        this.value *= modifier;
        CalculateEffect();

    }
    public Effect(Effect original)
    {
        this.reference = original.reference;
        this.TicksLeft = reference.ticks;
        this.SecondsLeft = reference.duration;
        usableAffected = original.usableAffected;
        affected = usableAffected;
        calc = original.calc;
        targetUsed = original.targetUsed;
        comparison = original.comparison;
        isOffensive = original.isOffensive;
        isAddable = original.isAddable;
        this.isAdditive = original.isAdditive;
        this.value = original.value;
        CalculateEffect();
    }
    public Effect(Effect original, float modifier) : this(original)
    {
        this.value = original.value * modifier;
    }

    public void Apply(Ship target, Action onFinished)
    {
        this.target = target;
        _continue = true;
        this.onFinished = onFinished;
        if (reference.duration > 0)
            this.target.BeginCoroutine(Decay);
        else
            target.SetStat(usableAffected, target.GetStat(usableAffected) + calculation(target));

    }
    public void Cancel() => _continue = false;
    private void CalculateEffect()
    {
        switch (calc)
        {
            case EffectCalculation.PercentageOf:
                calculation = (t) => (reference.isOffensive ? -1 : 1) * value * t.GetStat(targetUsed);
                break;
            case EffectCalculation.PercentageOfByStat:
                calculation = (t) => (reference.isOffensive ? -1 : 1) * PrecentageOfCalculation(t.GetStat(targetUsed), t.GetStat(comparison));
                break;
            case EffectCalculation.MissingPercentageOf:
                calculation = (t) => (reference.isOffensive ? -1 : 1) * MissingPrecentageOfCalculation(t.GetStat(targetUsed), t.GetStat(comparison));
                break;
            default:
                calculation = (t) => (reference.isOffensive ? -1 : 1) * value;
                break;
        }

    }
    private float MissingPrecentageOfCalculation(float a, float b) => value * (1 + (1 - (a / b)));
    private float PrecentageOfCalculation(float a, float b) => value * (1 + (a / b));
    private void OnModifierRemoveResponse(Statistic s, int i)
    {
        if (!removedModifier)
            if (this.usableAffected == s)
                if (this.referenceIndex > i)
                    this.referenceIndex--;
    }
    private void OnStatChangeResponse() => target.ChangeModifier(usableAffected, referenceIndex, calculation(target));

    private IEnumerator Decay()
    {
        if (isAddable)
        {
            target.OnModifierRemove += OnModifierRemoveResponse;
            referenceIndex = target.AddModifier(usableAffected, calculation(target));
            TicksLeft--;

            if (calc != EffectCalculation.Value)
            {
                target.OnStatChangeBind(targetUsed, OnStatChangeResponse);
                target.OnStatChangeBind(comparison, OnStatChangeResponse);
            }
        }
        float elapsedTime = 0;
        while (GameManager.IsRunning && _continue && SecondsLeft > 0)
        {
            while (!GameManager.IsPaused && _continue && SecondsLeft > 0)
            {

                elapsedTime += Time.deltaTime;
                SecondsLeft -= Time.deltaTime;
#if Debug
            secondsLeft = SecondsLeft;
#endif
                if (elapsedTime >= TickDelay)
                {
                    elapsedTime = 0;
                    if (TicksLeft > 0)
                    {
                        TicksLeft--;
#if Debug
                    ticksLeft = TicksLeft;
#endif
                        if (isAddable)
                            target.ChangeModifier(usableAffected, referenceIndex, calculation(target) * (isAdditive ? ((TicksLeft - reference.ticks) * -1) : 1));
                        else
                            target.SetStat(usableAffected, target.GetStat(usableAffected) + calculation(target) * (isAdditive ? ((TicksLeft - reference.ticks) * -1) : 1));
                    }
                }
                yield return new WaitForEndOfFrame();
            }
            yield return null;
        }

        if (isAddable)
        {
            removedModifier = true;
            target.RemoveModifier(usableAffected, referenceIndex);
            target.OnModifierRemove -= OnModifierRemoveResponse;
            if (calc != EffectCalculation.Value)
            {
                target.OnStatChangeUnbind(comparison, OnStatChangeResponse);
                target.OnStatChangeUnbind(targetUsed, OnStatChangeResponse);

            }
        }
        onFinished();
    }

}

