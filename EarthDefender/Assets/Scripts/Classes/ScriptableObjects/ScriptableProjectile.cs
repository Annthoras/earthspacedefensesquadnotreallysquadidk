﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;
[CreateAssetMenu(fileName = "Projectile", menuName = "Scriptables/Projectile", order = 2)]
public class ScriptableProjectile : ScriptableObject
{

    public Damage damage;
    public float flightSpeed;
    public List<ScriptableEffect> extraEffects = new List<ScriptableEffect>();
    public float explosionRadius;
    public Sprite sprite;
    public Color color = Color.white;
    public bool keepColor = false;
}

