﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using Extensions;
[CreateAssetMenu(fileName = "Invader", menuName = "Scriptables/Ships/Invader", order = 1)]
public class ScriptableInvader : ScriptableShip
{
    public override int Faction => 1;
    public InvaderFirePattern firePattern;
    public ScriptableInvader[] requiredNeighbors = new ScriptableInvader[4];
    public int burstAmmount;
    public float burstWait;
    public int fireChance;
    public float pickupChance;
    public List<ScriptablePickup> pickups = new List<ScriptablePickup>();
    public List<float> pickupChances = new List<float>();
    public int score;

    public override void FromSerialized(SerializableShip s)
    {
        base.FromSerialized(s);
        SerializableInvader inv = s as SerializableInvader;
        this.firePattern = inv.firePattern;
        this.requiredNeighbors = inv.requiredNeighbors.ForEach(x => Resources.Load<ScriptableInvader>(FileManager.ScriptableInvaders + x)).ToArray();
        this.burstAmmount = inv.burstAmmount;
        this.burstWait = inv.burstWait;
        this.fireChance = inv.fireChance;
        this.pickupChance = inv.pickupChance;
        this.pickups = inv.pickups.ForEach(x => { return Resources.Load<ScriptablePickup>(FileManager.ScriptablePickups + x); }).ToList();
        this.pickupChances = new List<float>(inv.pickupChances);
        this.score = inv.score;
    }

}

