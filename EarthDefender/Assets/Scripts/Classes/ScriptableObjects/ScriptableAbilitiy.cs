﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Extensions;
[CreateAssetMenu(fileName = "Ability", menuName = "Scriptables/Ability", order = 4)]
public class ScriptableAbilitiy : ScriptableObject
{
    public float duration;
    public AbilityType type;
    public float radius;
    public List<ScriptableEffect> effects = new List<ScriptableEffect>();
    public List<bool> delayedEffects = new List<bool>();
    public List<float> delayedEffectsDelays = new List<float>();
    public GameObject prefab;
    public Sprite sprite;
    public int energyCost;
    public float coolDown;

    public void FromSerialized(SerializableAbility a)
    {
        this.duration = a.duration;
        this.type = a.type;
        this.radius = a.radius;
    }

}

