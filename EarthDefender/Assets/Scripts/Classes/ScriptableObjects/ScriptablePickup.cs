﻿using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "Pickup", menuName = "Scriptables/Pickup", order = 5)]
public class ScriptablePickup : ScriptableObject
{
    public List<ScriptableEffect> effects = new List<ScriptableEffect>();
    public Sprite sprite;

}