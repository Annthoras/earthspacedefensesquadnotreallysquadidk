﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;
[CreateAssetMenu(fileName = "Weapon", menuName = "Scriptables/Weapon", order = 1)]
public class ScriptableWeapon : ScriptableObject
{
    public float roundsPerSecond;
    public float projectileFlightSpeed = 1;
    public int maxAmmo;
    public bool infiniteAmmo;
    public ScriptableProjectile usedProjectile;
    public float maxDegrees;
    public Sprite sprite;
    public Color color;
    public bool keepColor;
    public float turnSpeed = 1;
    public WeaponType type;
    public float weaponSpread = 1;
    public int projectileAmmount;

    public void FromSerialized(SerializableWeapon w)
    {
        this.roundsPerSecond = w.roundsPerSecond;
        this.projectileFlightSpeed = w.projectileFlightSpeed;
        this.maxAmmo = w.maxAmmo;
        this.infiniteAmmo = w.infiniteAmmo;
        this.usedProjectile = Resources.Load<ScriptableProjectile>("Scriptables/Projectiles/"+w.usedProjectile);
        this.maxDegrees = w.maxDegrees;
        this.sprite = Resources.Load<Sprite>("Weapons/"+w.sprite);
        this.color = w.color;
        this.keepColor = w.keepColor;
        this.turnSpeed = w.turnSpeed;
        this.type = w.type;
        this.weaponSpread = w.weaponSpread;
        this.projectileAmmount = w.projectileAmmount;
    }
}

