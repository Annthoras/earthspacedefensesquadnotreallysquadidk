﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Extensions;
[CreateAssetMenu(fileName = "Effect", menuName = "Scriptables/Effect", order = 3)]
public class ScriptableEffect : ScriptableObject
{
    public List<EffectCompartment> compartments = new List<EffectCompartment>();
    public float duration;
    public int ticks;
    public bool isOffensive;
    public Sprite graphic;
    public bool isUnique;
    public List<ScriptableEffect> uniques = new List<ScriptableEffect>();

    public void FromSerialized(SerializableEffect e)
    {
        this.compartments = new List<EffectCompartment>(e.compartments);
        this.duration = e.duration;
        this.ticks = e.ticks;
        this.isOffensive = e.isOffensive;
        this.graphic = Resources.Load<Sprite>("Effects/" + e.spriteName);
        this.isUnique = e.isUnique;
        this.uniques = new List<ScriptableEffect>();
        for (int i = 0; i < e.uniques.Count; i++)
        {
            uniques.Add(Resources.Load<ScriptableEffect>("Scriptables/Effects/" + (isOffensive ? "Debuffs/" : "Buffs/" + e.uniques[i])));
        }
    }
}

