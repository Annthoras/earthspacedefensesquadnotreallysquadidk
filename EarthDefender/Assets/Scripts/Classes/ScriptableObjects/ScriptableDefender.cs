﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Extensions;
[CreateAssetMenu(fileName = "Defender", menuName = "Scriptables/Ships/Defender", order = 0)]
public class ScriptableDefender : ScriptableShip
{
    public override int Faction => 0;
    public List<ScriptableWeapon> weapons = new List<ScriptableWeapon>();


    public override void FromSerialized(SerializableShip s)
    {
        SerializableDefender def = s as SerializableDefender;
        if (def == null)
            throw new ArgumentException("Given must be of type SerializableDefender");
        base.FromSerialized(s);
        this.weapons = def.weapons.ForEach(x => { return Resources.Load<ScriptableWeapon>(FileManager.ScriptableWeapons + x); }).ToList();
    }
}

