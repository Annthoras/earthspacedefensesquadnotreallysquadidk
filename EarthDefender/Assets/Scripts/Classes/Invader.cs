﻿#define Test
#define Debug
#if Debug
#define Debug_Explicit
#endif
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using Extensions;
public class Invader : Ship, IRayHitable
{
    public GameObject target;
    private int moveDir = -1;
    private GameObject barParent;
    private Image hpBar;
    private Image shieldBar;
    [SerializeField]
    private int remainingBurst;
    private int RemainingBurst
    {
        get => remainingBurst; set
        {
            remainingBurst = value;


        }
    }
#if Debug
    [SerializeField]
#endif
    private bool canFire;
    private SerializableInvader referenceInvader;
    private Action onFixedUpdate;
    System.Random rng;
    private Action fireAction;
    private List<ScriptablePickup> pickups;
    public static Invader prefab { get => Resources.Load<Invader>("Ships/Invaders/Invader"); }

    public static Invader Create(SerializableInvader reference, Vector2 position)
    {
        Invader result = GameManager.invaders.FirstOrDefault(x => !x.gameObject.activeSelf);
        if (result == null)
        {
            result = Instantiate<Invader>(prefab, InvasionManager.main.transform);
            GameManager.invaders.Add(result);
        }
        result.OnCreate();
#if !Test
        result.reference = reference;
#else
        result.Init(reference);
#endif
        result.transform.localPosition = position;
        return result;
    }
    public override void Init(SerializableShip reference)
    {
        base.Init(reference);
        canFire = false;
        referenceInvader = this.Reference as SerializableInvader;
        if (this.transform.position.x > 0)
            this.moveDir = 1;
        rng = new System.Random(UnityEngine.Random.Range(0, 18657));
        this.RemainingBurst = referenceInvader.burstAmmount;
        switch (referenceInvader.firePattern)
        {
            case InvaderFirePattern.Continuous:
                fireAction = () =>
                {
                    if (canFire)
                        Fire();
                };
                break;
            case InvaderFirePattern.Burst:
                fireAction = () =>
                {
                    if (canFire && weapon.canFire)
                    {
                        RemainingBurst--;
                        Fire();
                        if (RemainingBurst == 0)
                        {
                            canFire = false;
                            BeginCoroutine(BurstPause);
                        }

                    }
                };
                break;
            case InvaderFirePattern.Random:
                fireAction = () =>
                {
                    if (canFire)
                    {
                        if (rng.Next(100) < referenceInvader.fireChance)
                            Fire();
                    }
                };
                break;
        }
        if (referenceInvader.turretTurnSpeed > 0)
            onFixedUpdate = delegate
            {
                if (target != null)
                    this.LookAt(target.transform.position);

                if (!this.Move(new Vector2Int(moveDir, 0)))
                    moveDir *= -1;
            };
        else onFixedUpdate = delegate
        {
            if (!this.Move(new Vector2Int(moveDir, 0)))
                moveDir *= -1;
        };
        this.turret.rotation = new Quaternion(180, 0, 0, 0);
        this.pickups = new List<ScriptablePickup>(referenceInvader.pickups.ForEach(x => { return Resources.Load<ScriptablePickup>(FileManager.ScriptablePickups + x); }));
        this.gameObject.SetActive(true);
        BeginCoroutine(BeginningDelay);
        GameManager.OnUpdate += OnUpdate;
        GameManager.OnFixedUpdate += onFixedUpdate;
    }
    protected override void OnCreate()
    {
        base.OnCreate();
        barParent = this.transform.Find("HpCanvas").Find("TargetHP").gameObject;
        hpBar = barParent.transform.Find("HPFill").GetComponent<Image>();
        shieldBar = barParent.transform.Find("ShieldFill").GetComponent<Image>();
        this.Faction = 1;
        this.target = Defender.main.gameObject;
    }

    private void Start()
    {

    }
    public override void LookAt(Vector3 target)
    {
        Vector3 dir = target - turret.transform.position;
        float angle = Mathf.Atan2(dir.x, dir.y) * Mathf.Rad2Deg;
        float startAngle = angle;
        if (angle > 0 && angle < 180 - CurrentWeapon.maxDegrees)
            angle = 180 - CurrentWeapon.maxDegrees;
        else if (angle < 0 && angle > -180 + CurrentWeapon.maxDegrees)
            angle = -180 + CurrentWeapon.maxDegrees;
        Quaternion q = Quaternion.AngleAxis(-angle, Vector3.forward);
        turret.rotation = Quaternion.Slerp(turret.rotation, q, Time.deltaTime * this.TurretTurnSpeed * CurrentWeapon.turnSpeed);
    }
    public void OnRayHit()
    {
        barParent.SetActive(true);
        hpBar.fillAmount = this.Health / this.MaxHealth;
        shieldBar.fillAmount = this.Shield / this.MaxShield;
    }
    public void OnRayLeave()
    {
        barParent.SetActive(false);
    }
    private void OnUpdate()
    {
        int oldBurst = remainingBurst;
        fireAction();
        if (remainingBurst < 0 && remainingBurst != oldBurst)
        {

        }
    }

    protected override void Die()
    {
        InvasionManager.RemoveInvader(this);
        GameManager.OnFixedUpdate -= onFixedUpdate;
        GameManager.OnUpdate -= OnUpdate;
        Pickup p = Pickup.Create();
        if (referenceInvader.pickups.Count > 0)
        {
            int chance = rng.Next(0, 100);
            if (chance / 100f <= referenceInvader.pickupChance)
            {
                chance = rng.Next(0, 100);
                int result = 0;
                float sum = 0;
                for (int i = 0; i < referenceInvader.pickupChances.Count; i++)
                {
                    sum += referenceInvader.pickupChances[i];
                    if (sum >= chance / 100f)
                    {
                        result = i;
                        break;
                    }
                }
                p.Init(pickups[result], this.transform.position);
            }
        }
        GameManager.Score += referenceInvader.score;
        this.gameObject.SetActive(false);
    }

    private IEnumerator BeginningDelay()
    {
        int delay = rng.Next(1, 3);
        float secsLeft = delay;
        while (GameManager.IsRunning && secsLeft > 0)
        {
            while ((!GameManager.IsPaused) && secsLeft > 0)
            {
                secsLeft -= Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }
            yield return null;
        }
        canFire = true;
    }
    private IEnumerator BurstPause()
    {
        float pause = referenceInvader.burstWait + UnityEngine.Random.Range(-0.3f, 0.3f);
        float secsLeft = pause;
        while (GameManager.IsRunning && secsLeft > 0)
        {
            while ((!GameManager.IsPaused) && secsLeft > 0)
            {
                secsLeft -= Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }
            yield return null;
        }
        RemainingBurst = referenceInvader.burstAmmount;
        canFire = true;

    }
}