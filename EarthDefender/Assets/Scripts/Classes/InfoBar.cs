﻿#define Debug

using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class InfoBar : MonoBehaviour
{
    #region Encapsulated Variables
    private bool hideText;
    private bool hideImage;
    #endregion
    Image image;
    public bool updateImage;
    public bool HideText
    {
        get => hideText;
        set {
            hideText = value;
            text.gameObject.SetActive(!value);
        }
    }
    public bool HideImage
    {
        get => hideImage;
        set
        {
            hideImage = value;
            image.gameObject.SetActive(!value);
        }
    }

    public Statistic maxStat;
    public Statistic stat;
    TextMeshProUGUI text;
    float currentMaxStat;
    float currentStat;
    public Action Refresh;

    public void Init(Ship target)
    {
        this.image = this.transform.Find("Fill").GetComponent<Image>();
        this.text = this.transform.Find("Text").GetComponent<TextMeshProUGUI>();

        if (updateImage)
            Refresh = () =>
            {
                this.currentMaxStat = target.GetStat(maxStat);
                this.currentStat = target.GetStat(stat);
                this.image.fillAmount = currentStat / currentMaxStat;
                this.text.text = string.Format("{0}/{1}", (int)currentStat, (int)currentMaxStat);
            };
        else Refresh = () =>
        {

            this.currentMaxStat = target.GetStat(maxStat);
            this.currentStat = target.GetStat(stat);
            if (currentMaxStat == -1)
                this.text.text = "";
            else
            this.text.text = string.Format("{0}/{1}", (int)currentStat, (int)currentMaxStat);
        };
        Refresh();
    }
}

