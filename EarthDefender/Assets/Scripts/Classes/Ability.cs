﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Extensions;
public class Ability
{
    public ScriptableAbilitiy reference;
    public Ship owner;
    public float duration;
    public float radius;
    public Action activate;
    public float CoolDown
    {
        get
        {
            return reference.coolDown;
        }
    }
    public float CurrentCooldown { get => _currentCoolDown; }
    private float _currentCoolDown;
    private Action onActivate;
    private Action onCoolDownFinish = () => { };
    public Ability(Ship owner, ScriptableAbilitiy reference)
    {
        this.reference = reference;
        this.owner = owner;
        this.duration = reference.duration;
        this.radius = reference.radius;
        switch (reference.type)
        {
            case AbilityType.Effect:

                activate = () =>
                {
                    for (int i = 0; i < reference.effects.Count; i++)
                    {
                        ScriptableEffect current = reference.effects[i];
                        if (reference.delayedEffects[i])
                            owner.BeginCoroutine(DelayedApply, current, reference.delayedEffectsDelays[i]);
                        else
                            ApplyCompartments(current);
                    }
                };
                break;
        }
        onActivate = () =>
        {
            _currentCoolDown = CoolDown;
            GameManager.main.StartCoroutine(CoolDownWait());
            activate();
            owner.SetStat(Statistic.Energy, owner.GetStat(Statistic.Energy) - reference.energyCost);
        };
    }
    public void OnActivateBind(Action a) => onActivate += a;
    public void OnActivateUnbind(Action a) => onActivate -= a;
    public void OnCoolDownFinishBind(Action a) => onCoolDownFinish += a;
    public void OnCoolDownFinishUnbind(Action a) => onCoolDownFinish -= a;
    private IEnumerator CoolDownWait()
    {
        while (_currentCoolDown > 0)
        {
            _currentCoolDown -= Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        onCoolDownFinish();
    }
    private IEnumerator DelayedApply(ScriptableEffect effect, float delay)
    {
        float remaining = delay;
        while (GameManager.IsRunning && remaining > 0)
        {
            while (GameManager.IsRunning && remaining > 0)
            {
                yield return new WaitForEndOfFrame();
                remaining -= Time.deltaTime;
            }
            yield return null;
        }
        ApplyCompartments(effect);
    }
    private void ApplyCompartments(ScriptableEffect effect)
    {
        effect.compartments.ForEach(c =>
        {
            if (effect.isOffensive)
                owner.ApplyEffectToWeapon(new Effect(effect, c, owner.DamageModifier), this.duration);
            else
                owner.ApplyEffect(new Effect(effect, c, owner.DamageModifier));
        });
    }
    public void Activate()
    {
        if (_currentCoolDown > 0)
            return;
        onActivate();
    }

}

