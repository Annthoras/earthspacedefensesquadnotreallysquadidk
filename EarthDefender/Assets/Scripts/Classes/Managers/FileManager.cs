﻿//#define SelfMade
using System;
using System.IO;
using System.Reflection;
using System.Text;
using Extensions;
using Newtonsoft.Json;
using UnityEngine;
public static class FileManager
{
    #region Resource Paths
    public static string Effects => "Effects/";
    public static string Pickups => "Pickups/";
    public static string Projectiles => "Projectiles/";
    public static string Ships => "Ships/";
    public static string Weapons => "Weapons/";
    public static string Abilities => "Abilities/";
    public static string Invaders => "Ships/Invaders/";
    public static string Defenders => "Ships/Defenders/";
    public static string Scriptables => "Scriptables/";
    public static string ScriptableShips => "Scriptables/Ships/";
    public static string ScriptableDefenders => "Scriptables/Ships/Defenders/";
    public static string ScriptableInvaders => "Scriptables/Ships/Invaders/";
    public static string ScriptableBosses => "Scriptables/Ships/Invaders/Bosses/";
    public static string ScriptableWeapons => "Scriptables/Weapons/";
    public static string ScriptableEffects => "Scriptables/Effects/";
    public static string ScriptableBuffs => "Scriptables/Effects/Buffs/";
    public static string ScriptableDebuffs => "Scriptables/Effects/Debuffs/";
    public static string PickupEffects => "Scriptables/Effects/Buffs/Pickups/";
    public static string ScriptablePickups => "Scriptables/Pickups/";
    public static string ScriptableProjectiles => "Scriptables/Projectiles/";
    public static string ScriptableAbilities => "Scriptables/Abilities/";



    #endregion
    private static string _root = "!";
    public static string Root
    {
        get
        {
            if (_root == "!")
            {
                Assembly asm = Assembly.GetExecutingAssembly();
                _root = asm.Location.Replace(@"Library\ScriptAssemblies\" + asm.GetName().Name + ".dll", "");
            }
            return _root;
        }
    }



    public static void WriteToFile(string fileName, IWriteable writeable, string extension)
    {
        string path = Root + "\\" + fileName + "." + extension;
        FileStream stream;
        if (!File.Exists(path))
        {
            File.Delete(path);
            stream = File.Create(path);
            stream.Close();
        }
        stream = File.Open(path, FileMode.Open);
        string data = writeable.Write();
        stream.Write(new UTF8Encoding().GetBytes(data), 0, data.Length);
        stream.Close();

    }
    public static T ReadFromFile<T>(string fileName) where T: IReadable<T>
    {
        string path = Root + "\\" + fileName;
        FileStream stream = File.OpenRead(path);
        byte[] data = new byte[1024];
        stream.Read(data, 0, data.Length);
        stream.Close();
        return JsonConvert.DeserializeObject<T>(new UTF8Encoding().GetString(data));
    }

    


#if SelfMade
    public static string ReadFromFile<T>(string fileName) where T : IReadable<T>
    {
        string path = Root + "\\" + fileName;
        FileStream stream = File.OpenRead(path);
        byte[] data = new byte[1024];
        stream.Read(data, 0, data.Length);
        stream.Close();
        return new UTF8Encoding().GetString(data).Trim('\0');
    }
    /// <summary>
    /// Attempts to parse the value of the given object from data
    /// </summary>
    /// <param name="objectName">The object that should be parsed</param>
    /// <param name="data">The data from which the object should be parsed</param>
    /// <param name="usedIndex">Used for skipping sections which do not contain the desired object</param>
    /// <returns>
    /// The value of the object, if it could be found. Otherwise returns "NaN"
    /// </returns>
    public static string ParseObject(string objectName, string data, ref int usedIndex)
    {
        //determine if the wanted object exists in data
        if (!data.ToLower().TrimAll(' ').Contains(objectName.ToLower().TrimAll(' ')))
            return "NaN";
        string result = "";
        string foundName = "";
        char endChar = ';';
        //used to know if the value of the object has been reached
        bool lookingAtValue = false;
        //used to know if the object is made of more objects
        bool isConstruct = false;
        int depth = 0;
        for (int i = usedIndex; i < data.Length; i++)
        {
            if (lookingAtValue)
            {
                if (isConstruct)
                {
                    //if { appears, then we know the wanted object has complex children
                    if (data[i] == '{')
                        depth++;
                    //if } appears, then we know we're exiting a complex object
                    else if (data[i] == '}')
                        depth--;
                }
                if (data[i] == endChar && (isConstruct || (isConstruct && depth <= 0)))
                {
                    usedIndex = i;
                    break;
                }
                result += data[i];
            }
            else
            {
                //this is where the value of the object should begin
                if (data[i] == ':')
                {
                    //check if we found the right object
                    if (Trim(foundName.ToLower()) == Trim(objectName.ToLower()))
                    {
                        lookingAtValue = true;
                        //check to see if it is a complex object
                        if (data[i + 1] == '{')
                        {
                            i += 1;
                            isConstruct = true;
                            depth++;
                            endChar = '}';
                        }
                    }
                }
                else
                {
                    //if we have found the value of another object, discard it
                    if (data[i] == ';' || data[i] == '}')
                        foundName = "";
                    else
                        foundName += data[i];
                }

            }

        }
        return result;
    }


    public static string Trim(string input)
    {

        return input.TrimAll('\n', '{', '}', ';', ':', ' ');
    }
#endif

}