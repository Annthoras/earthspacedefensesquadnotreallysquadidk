﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
public class InvasionManager : MonoBehaviour
{
    public static InvasionManager main { get; private set; }
    private Invader[,] invaders = new Invader[13, 5];
    private ScriptableInvader[,] nextWave = new ScriptableInvader[13, 5];
    public int startingNumber = 0;
    private System.Random rng;
    public int wave;
    private int invaderNumber = 0;
    public ScriptableInvader[] references { get => Resources.LoadAll<ScriptableInvader>("Scriptables/Ships/Invaders"); }
    private ScriptableInvader[] noLeft;
    private ScriptableInvader[] noRight;
    private ScriptableInvader[] noUp;
    private ScriptableInvader[] noDown;
    private void Awake()
    {
        InvasionManager other = FindObjectOfType<InvasionManager>();
        if (other != null && other != this && this != main)
            Destroy(this);
        main = this;
    }
    public void Start()
    {
        noLeft = references.Where(x => x.requiredNeighbors[2] == null).ToArray();
        noRight = references.Where(x => x.requiredNeighbors[1] == null).ToArray();
        noUp = references.Where(x => x.requiredNeighbors[0] == null).ToArray();
        noDown = references.Where(x => x.requiredNeighbors[3] == null).ToArray();
        rng = new System.Random(DateTime.Now.Millisecond);
        CalculateNextWave();
        SpawnNextWave();
    }

    private void CalculateNextWave()
    {
        nextWave = new ScriptableInvader[13, 5];
        int nextAmmount = startingNumber + wave;
        if (nextAmmount > 65) nextAmmount = 65;
        int fullRows = (nextAmmount - (nextAmmount % 13)) / 13;
        int remainingInvaders = nextAmmount - (fullRows * 13);
        int actualRows = remainingInvaders > 0 ? fullRows + 1 : fullRows;
        for (int y = 0; y < actualRows; y++)
        {
            int currentInvaders = y == 0 ? (remainingInvaders > 0 ? remainingInvaders : 13) : 13;
            int minX;
            int maxX;
            bool skip0 = false;
            if (currentInvaders % 2 == 1)
            {
                minX = 6 - ((currentInvaders - 1) / 2);
                maxX = 6 + ((currentInvaders - 1) / 2);
            }
            else
            {
                minX = 6 - (currentInvaders / 2);
                maxX = 6 + (currentInvaders / 2);
                skip0 = true;
            }
            for (int x = minX; x <= maxX; x++)
            {
                if (x == 6 && skip0)
                    x++;
                if (nextWave[x, y] == null)
                {
                    List<ScriptableInvader> pickables = new List<ScriptableInvader>(references);
                    if (y == actualRows - 1)
                        pickables.RemoveAll(sI => !noUp.Contains(sI));
                    if (y == 0 || nextWave[x, y - 1] == null)
                        pickables.RemoveAll(sI => !noDown.Contains(sI));
                    if (x == minX)
                        pickables.RemoveAll(sI => !noLeft.Contains(sI));
                    else if (x == maxX)
                        pickables.RemoveAll(sI => !noRight.Contains(sI));
                    ScriptableInvader next = pickables[rng.Next(0, pickables.Count)];
                    nextWave[x, y] = next;
                    if (next.requiredNeighbors[3] != null && y > 0 && nextWave[x, y - 1] != next.requiredNeighbors[3])
                        nextWave[x, y - 1] = next.requiredNeighbors[3];
                    if (next.requiredNeighbors[0] != null && y < actualRows && nextWave[x, y + 1] != next.requiredNeighbors[0])
                        nextWave[x, y + 1] = next.requiredNeighbors[0];
                    if (next.requiredNeighbors[1] != null && x > minX && nextWave[x - 1, y] != next.requiredNeighbors[1])
                        nextWave[x - 1, y] = next.requiredNeighbors[1];
                    if (next.requiredNeighbors[2] != null && x < maxX && nextWave[x + 1, y] != next.requiredNeighbors[2])
                        nextWave[x + 1, y] = next.requiredNeighbors[2];
                }
            }
        }
    }
    private void SpawnNextWave()
    {
        int nextAmmount = startingNumber + wave;
        wave++;
        int fullRows = (nextAmmount - (nextAmmount % 13)) / 13;
        int remainingInvaders = nextAmmount - (fullRows * 13);
        int actualRows = remainingInvaders > 0 ? fullRows + 1 : fullRows;
        invaders = new Invader[13, 5];
        for (int y = 0; y < actualRows; y++)
        {
            for (int x = 0; x < 13; x++)
            {
                if (nextWave[x, y] != null)
                {

                    invaders[x, y] = Invader.Create(new SerializableInvader(nextWave[x, y]), new Vector2((-12 + x * 2), -4 + y * 2));
                    invaderNumber++;

                }
            }
        }
        CalculateNextWave();
    }
    public static void RemoveInvader(Invader obj)
    {
        bool breakX = false;
        for (int x = 0; x < 13; x++)
        {
            for (int y = 0; y < 5; y++)
            {
                if (main.invaders[x, y] == obj)
                {
                    main.invaders[x, y] = null;
                    main.invaderNumber--;
                    breakX = true;
                    break;
                }
            }
            if (breakX)
                break;
        }
        if (main.invaderNumber == 0)
            main.SpawnNextWave();
    }




}

