﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Extensions;
public class InputManager : MonoBehaviour
{
    private static InputManager main;
    public static Dictionary<string, Tuple<KeyBinding, KeyBinding>> bindings;
    private Dictionary<string, Action> bindingFunctions = new Dictionary<string, Action>()
    {
        { "left",()=>Defender.MoveMain(Vector2Int.left) },
        { "right",()=>Defender.MoveMain(Vector2Int.right) },
        { "fire",()=>Defender.FireMain() },
        { "pause",()=>GameManager.IsPaused = !GameManager.IsPaused },
        //{ "altFire",()=>Defender.AltFire() },
    };
    private List<ScriptableWeapon> weapons = new List<ScriptableWeapon>();
    private List<ScriptableAbilitiy> abilities = new List<ScriptableAbilitiy>();
    private void Awake()
    {
        InputManager first = FindObjectOfType<InputManager>();
        if (first != this)
            Destroy(this);
        main = this;
        bindings = new Dictionary<string, Tuple<KeyBinding, KeyBinding>>();

    }
    private void Start()
    {
        UseDefault();
        weapons = Resources.LoadAll<ScriptableWeapon>("Scriptables/Weapons").ToList();
        abilities = Defender.main.abilities.ForEach(x => x.reference).ToList();
        weapons.ForEach(x => bindingFunctions.Add(x.name, () => Defender.SwitchWeapon(x)));
        int abilNum = 1;
        abilities.ForEach(x =>
        {
            bindingFunctions.Add("Ability" + abilNum, () => Defender.UseAbility(x));
            abilNum++;
        });
        BindFunctions();

    }
    private void BindFunctions()
    {

        IEnumerable<KeyValuePair<string, Tuple<KeyBinding, KeyBinding>>> dictionaryWithoutPause = bindings.Where(x => x.Key != "pause");
        GameManager.OnUpdate += () =>
        {
            dictionaryWithoutPause.ForEach(x =>
            {
                if (x.Value.Item1.check() || x.Value.Item2.check())
                    bindingFunctions[x.Key]();
            });
        };
    }

    private void UseDefault()
    {
        ReadLine("left = A (h);LeftArrow (h)");
        ReadLine("right = D (h);RightArrow (h)");
        ReadLine("fire = m0 (h)");
        ReadLine("PeaShooter = 1");
        ReadLine("MachineGun = 2");
        ReadLine("Shotgun = 3");
        ReadLine("RocketLauncher = 4");
        ReadLine("Ability1 = Q");
        ReadLine("Ability2 = E");
        ReadLine("Abilitiy3 = R");
        ReadLine("pause = Escape;P");
        //ReadLine("altFire = m1");
    }
    private void Update()
    {
        if (bindings["pause"].Item1.check() || bindings["pause"].Item1.check())
            bindingFunctions["pause"]();
    }
    private KeyBinding ReadKeybind(string name, string bind)
    {
        KeyBinding result = new KeyBinding();
        bind += ' ';
        string currentConstruct = "";
        bool hitParenthesis = false;
        for (int i = 0; i < bind.Length; i++)
        {
            char c = bind[i];
            if (c == '+')
            {
                result.mod = (KeyCode)Enum.Parse(typeof(KeyCode), currentConstruct);
                currentConstruct = "";
            }
            else if (c == '(' || (c == ' ' && !hitParenthesis))
            {
                hitParenthesis = true;
                if (currentConstruct[0] == 'm')
                    result.mouseButton = currentConstruct[1] - 48;
                else
                {
                    if (currentConstruct[0] - 48 >= 0 && currentConstruct[0] - 48 < 10)
                        currentConstruct = "Alpha" + currentConstruct[0];
                    result.key = (KeyCode)Enum.Parse(typeof(KeyCode), currentConstruct);
                }
                currentConstruct = "";
            }
            else if (c == ')')
            {
                if (currentConstruct[0] == 'h')
                    result.onHold = true;
                else if (currentConstruct[0] == 'u')
                    result.onUp = true;
                else throw new FormatException("Acceptable characters are either \"h\" or \"u\"");
            }
            if (c != '+' && c != '(' && c != ')')
                currentConstruct += c;
        }
        return result;
    }
    //Read Document function
    private void ReadLine(string line)
    {
        line = line.Replace(" ", "");
        string[] split = line.Split('=');
        string name = split[0];
        List<KeyBinding> binds = split[1].Split(';').ForEach(x => ReadKeybind(name, x)).ToList();
        if (binds.Count < 2)
            binds.Add(KeyBinding.Empty);
        binds.ForEach(x => x.ConstructCheck());
        AddBinding(name, binds.ToArray());
    }

    public static void AddBinding(string name, KeyBinding binding)
    {
        if (!bindings.ContainsKey(name))
            bindings.Add(name, new Tuple<KeyBinding, KeyBinding>(new KeyBinding(), new KeyBinding()));
        if (bindings[name].Item1 == KeyBinding.Empty)
            bindings[name] = new Tuple<KeyBinding, KeyBinding>(binding, bindings[name].Item2);
        else bindings[name] = new Tuple<KeyBinding, KeyBinding>(bindings[name].Item1, binding);
    }
    public static void AddBinding(string name, params KeyBinding[] bindings) => bindings.ForEach(x => AddBinding(name, x));

    public static void RemoveBinding(string name, KeyBinding binding) => ReplaceBinding(name, binding, KeyBinding.Empty);

    public static void ReplaceBinding(string name, KeyBinding oldBinding, KeyBinding newBinding)
    {
        if (oldBinding == bindings[name].Item1)
            bindings[name] = new Tuple<KeyBinding, KeyBinding>(newBinding, bindings[name].Item2);
        else if (oldBinding == bindings[name].Item2)
            bindings[name] = new Tuple<KeyBinding, KeyBinding>(bindings[name].Item1, newBinding);

    }

}

