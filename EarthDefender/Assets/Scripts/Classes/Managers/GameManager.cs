﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager main;
    private static bool _isRunning = true;
    private static bool _isPaused = false;
    private static int _score = 0;
    /// <summary>
    /// False means the game is paused
    /// </summary>
    public static bool IsPaused
    {
        get => _isPaused;
        set
        {
            if (_isPaused && !value)
                OnGameResume();
            else if (!_isPaused && value)
                OnGamePause();
            _isPaused = value;
        }
    }
    public static bool IsRunning { get => _isRunning; private set => _isRunning = value; }
    public static event Action OnUpdate = delegate { };
    public static event Action OnFixedUpdate = delegate { };
    public static event Action OnGamePause = delegate { };
    public static event Action OnGameResume = delegate { };
    public static List<Invader> invaders = new List<Invader>();
    public static List<Projectile> projectiles = new List<Projectile>();
    public static List<Pickup> pickups = new List<Pickup>();
    private static Action onScoreChanged = delegate { };
    public static int Score
    {
        get => _score;
        set
        {
            _score = value;
            onScoreChanged();
        }
    }
    public string defender;
    private void Awake()
    {
        GameManager other = FindObjectOfType<GameManager>();
        if (other != null && other != this && this != main)
            Destroy(this);
        main = this;
        Defender.main.Init(new CustomizedShip(Resources.Load<ScriptableDefender>(FileManager.ScriptableDefenders + defender)));
    }
    public static void OnScoreChangeBind(Action act) => onScoreChanged += act;
    public static void OnScoreChangeUnBind(Action act) => onScoreChanged -= act;

    private void Start()
    {
    }

    private void Update()
    {
        if (!IsPaused)
            OnUpdate();
    }
    private void FixedUpdate()
    {
        if (!IsPaused)
            OnFixedUpdate();
    }

}

