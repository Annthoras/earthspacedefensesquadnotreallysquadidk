﻿#define Debug
#define Debug_Explicit
#define Test
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Extensions;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class GameUIManager : UIManager
{
    private InfoBar[] bars = new InfoBar[5] { null, null, null, null, null };
    public GameObject buffArea;
    public GameObject debuffArea;
    private Transform gameUI;
    private Transform pauseUI;
    private Transform abilityArea;
    private TextMeshProUGUI scoreText;
    public List<UIAbility> abilities = new List<UIAbility>();
    public List<UIEffect> buffs = new List<UIEffect>();
    public List<UIEffect> debuffs = new List<UIEffect>();
    private static UIEffect GetEffectPrefab(bool isOffensive) => Resources.Load<UIEffect>("UI/Prefabs/" + (isOffensive ? "Debuff" : "Buff") + "Effect");
    private static Transform GetEffectArea(bool isOffensive) => isOffensive ? (current as GameUIManager).debuffArea.transform : (current as GameUIManager).buffArea.transform;
    protected override void Start()
    {
        base.Start();
        gameUI = this.transform.Find("GameUI");
        pauseUI = this.transform.Find("PauseUI");
        abilityArea = gameUI.Find("AbilityBar");
        #region GameUI
        bars[0] = gameUI.transform.Find("HPBar").GetComponent<InfoBar>();
        bars[1] = gameUI.transform.Find("ShieldBar").GetComponent<InfoBar>();
        bars[2] = gameUI.transform.Find("ArmorBar").GetComponent<InfoBar>();
        bars[3] = gameUI.transform.Find("EnergyBar").GetComponent<InfoBar>();
        bars[4] = gameUI.transform.Find("AmmoCount").GetComponent<InfoBar>();
        scoreText = gameUI.transform.Find("Score").GetComponent<TextMeshProUGUI>();
        #endregion
        bars.ForEach(x =>
        {
            x.Init(Defender.main);
            Defender.main.OnStatChangeBind(x.stat, x.Refresh);
            Defender.main.OnStatChangeBind(x.maxStat, x.Refresh);
        });
        int numAbils = 0;
        Defender.main.abilities.ForEach(a =>
        {
            UIAbility newAbil = Instantiate<UIAbility>(Resources.Load<UIAbility>("UI/Prefabs/Ability"));
            newAbil.transform.SetParent(abilityArea, false);
            newAbil.transform.localPosition = new Vector3(-70 + (37 * numAbils), 0, 0);
            newAbil.Init(a);
            numAbils++;
        });
        Defender.main.OnEffectApply += SpawnEffect;
        Defender.main.OnWeaponEffectApply += SpawnWeaponEffect;
        GameManager.OnGamePause += OnPauseButtonResponse;
        GameManager.OnGameResume += OnResumeGameResponse;
        GameManager.OnScoreChangeBind(() => scoreText.text = GameManager.Score.ToString());

    }


    private void SpawnWeaponEffect(Effect e, float duration)
    {
        if (buffs.Any(x => x.effect == e.reference))
        {
            buffs.First(x => x.effect == e.reference).OverrideStack(e, duration);
            return;
        }
        UIEffect newEffect = Instantiate(GetEffectPrefab(false));
        newEffect.transform.SetParent(GetEffectArea(false));
        newEffect.transform.localScale = new Vector3(1, 1, 1);
        newEffect.transform.localPosition = buffs.Count > 0 ? (buffs[buffs.Count - 1].transform.localPosition - new Vector3(0, 45, 0)) : new Vector3(30, -30, 0);
        buffs.Add(newEffect);
        newEffect.Init(e.reference, true);
        newEffect.Icon.sprite = e.reference.graphic;
        newEffect.gameObject.SetActive(true);
        newEffect.OverrideStack(e, duration);
    }

    private void SpawnEffect(Effect e)
    {
        bool isOffensive = e.reference.isOffensive;
        List<UIEffect> effectList = isOffensive ? debuffs : buffs;
        if (effectList.Any(x => x.effect == e.reference))
        {
            effectList.First(x => x.effect == e.reference).AddStack(e);
            return;
        }
        Vector3 offset = new Vector3(isOffensive ? -30 : 30, -30, 0);
        UIEffect newEffect = Instantiate(GetEffectPrefab(isOffensive));
        newEffect.transform.SetParent(GetEffectArea(isOffensive));
        newEffect.transform.localPosition = effectList.Count > 0 ? (effectList[effectList.Count - 1].transform.localPosition - new Vector3(0, 45, 0)) : offset;
        effectList.Add(newEffect);
        newEffect.transform.localScale = new Vector3(1, 1, 1);
        newEffect.Init(e.reference, false);

        newEffect.Icon.sprite = e.reference.graphic;
        newEffect.gameObject.SetActive(true);
        newEffect.AddStack(e);
    }

    public static void RemoveEffect(UIEffect e)
    {
        List<UIEffect> effectlist = e.isWeaponEffect ? (current as GameUIManager).buffs : (e.effect.isOffensive ? (current as GameUIManager).debuffs : (current as GameUIManager).buffs);
        int index = effectlist.IndexOf(e);
        for (int i = index; i < effectlist.Count; i++)
            effectlist[i].transform.localPosition += new Vector3(0, 45, 0);
        effectlist.Remove(e);
        Destroy(e.gameObject);
    }

    public void OnPauseButtonResponse()
    {
        gameUI.gameObject.SetActive(false);
        pauseUI.gameObject.SetActive(true);
    }
    public void ResumeGame()
    {
        GameManager.IsPaused = false;
    }
    private void OnResumeGameResponse()
    {
        pauseUI.gameObject.SetActive(false);
        optionsUI.gameObject.SetActive(false);
        gameUI.gameObject.SetActive(true);
    }
    public override void OptionsMenu()
    {
        pauseUI.gameObject.SetActive(false);
        optionsUI.gameObject.SetActive(true);
    }
    public void Restart()
    {

    }
    public void HelpMenu()
    {
        pauseUI.gameObject.SetActive(false);
        //helpUI.gameObject.SetActive(true);
    }
    public void ReturnToPauseMenu()
    {
        //helpUI.gameObject.SetActive(false);
        optionsUI.gameObject.SetActive(false);
        pauseUI.gameObject.SetActive(true);
    }


    public void MainMenu()
    {
        //Change scene to main menu
    }

}
