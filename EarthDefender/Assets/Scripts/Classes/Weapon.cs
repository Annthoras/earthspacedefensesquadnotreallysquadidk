﻿#define Test
#define Debug
#if Debug
#define Debug_Explicit
#endif
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Extensions;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    public SpriteRenderer Renderer { get; private set; }
    private ScriptableWeapon reference;
    public ScriptableWeapon Reference
    {
        get => reference; set
        {
            reference = value;
            Renderer.sprite = reference.sprite;
            Renderer.color = reference.keepColor ? reference.color : owner.Reference.color;
            switch (Reference.type)
            {
                case WeaponType.Standard:
                    weaponTypeFire = () =>
                    {
                        if (canFire && ((owner.Faction == 1 || reference.infiniteAmmo) || owner.Ammo >= 1))
                        {
                            owner.Ammo--;
                            canFire = false;
                            SpawnProjectile(muzzle.up);
                            StartCoroutine(FireDelay());
                        }
                    };
                    break;
                case WeaponType.ShotgunLike:
                    weaponTypeFire = () =>
                    {
                        if (canFire && ((owner.Faction == 1 || reference.infiniteAmmo) || owner.Ammo >= reference.projectileAmmount))
                        {
                            owner.Ammo -= Reference.projectileAmmount;
                            canFire = false;
                            bool skip0 = Reference.projectileAmmount % 2 == 0;
                            for (int i = -(int)(Reference.projectileAmmount / 2f); i <= (int)(reference.projectileAmmount / 2f); i++)
                            {
                                if (i != 0 || !skip0)
                                {
                                    Vector2 projectileUp = i == 0 ? muzzle.up : (muzzle.up + (muzzle.right * (Reference.weaponSpread / i)));
                                    SpawnProjectile(projectileUp);
                                }
                            }
                            StartCoroutine(FireDelay());
                        }
                    };
                    break;
                case WeaponType.MultiWeapon:
                    break;
                default:
                    break;
            }
        }
    }
    public Ship owner;
#if Debug
    [SerializeField]
#endif
    private Transform muzzle;
    public Dictionary<Effect, float> extraEffects { get; private set; }

    private Action weaponTypeFire;
    private event Action<int> onEffectRemove = delegate { };
#if Debug
    [SerializeField]
#endif
    public bool canFire;
    private float ShotDelay
    {
        get => (1f / Reference.roundsPerSecond) / owner.FireRate;
    }
#if Debug
    public float remainingTime;
#endif
    public void Init()
    {
        canFire = true;
        this.Renderer = this.GetComponent<SpriteRenderer>();
        extraEffects = new Dictionary<Effect, float>();
        muzzle = this.transform.Find("Muzzle");
    }
    private void SpawnProjectile(Vector2 up)
    {
        Projectile spawned = Projectile.Create();
        spawned.transform.up = up;
        spawned.transform.position = muzzle.transform.position;
        spawned.Init(Reference.usedProjectile, this);
    }
    public void Fire() => weaponTypeFire();
    public void AddEffect(Effect e, float duration)
    {
        extraEffects.Add(e, duration);
        StartCoroutine(BuffDecay(e));

    }
    private IEnumerator BuffDecay(Effect e)
    {
        while (GameManager.IsRunning && extraEffects[e] > 0)
        {
            while (!GameManager.IsPaused && extraEffects[e] > 0)
            {
                extraEffects[e] -= Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }
            yield return null;
        }
        extraEffects.Remove(e);
    }
    private IEnumerator FireDelay()
    {
        float remainingTime = ShotDelay;
        bool breakUpper = false;
        while (GameManager.IsRunning && !canFire)
        {
            while (!GameManager.IsPaused && !canFire)
            {
                remainingTime -= Time.deltaTime;
                if (remainingTime > ShotDelay)
                    remainingTime = ShotDelay;
                if (remainingTime <= 0)
                {
                    canFire = true;
                    breakUpper = true;
                    break;
                }
                yield return new WaitForEndOfFrame();
            }
            if (breakUpper)
                break;
            yield return null;
        }
    }
}

