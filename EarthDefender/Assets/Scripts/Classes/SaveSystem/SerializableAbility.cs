﻿using System;
using System.Linq;
using System.Collections.Generic;
using Newtonsoft.Json;
using Extensions;
[Serializable]
public class SerializableAbility : IWriteable, IReadable<SerializableAbility>
{
    public float duration;
    public AbilityType type;
    public float radius;
    public List<SerializableEffect> effects;
    public List<bool> delayedEffect;
    public List<float> delayedEffectDelays;
    public string prefab;
    public string sprite;
    public int energyCost;
    public float coolDown;

    public SerializableAbility() { }
    public SerializableAbility(ScriptableAbilitiy a)
    {
        this.duration = a.duration;
        this.type = a.type;
        this.radius = a.radius;
        this.delayedEffect = new List<bool>(a.delayedEffects);
        this.delayedEffectDelays = new List<float>(a.delayedEffectsDelays);
        this.prefab = a.prefab.name;
        this.sprite = a.sprite.name;
        this.energyCost = a.energyCost;
        this.coolDown = a.coolDown;
    }


    public void Read(string input)
    {
        
    }

    public string Write()
    {
        return JsonConvert.SerializeObject(this);
    }
}