﻿using System;
using System.Linq;
using System.Collections.Generic;
using Extensions;
using Newtonsoft.Json;
[Serializable]
public class SerializableDefender : SerializableShip, IReadable<SerializableDefender>
{
    public List<string> weapons = new List<string>();
    public SerializableDefender()
    {

    }
    public SerializableDefender(ScriptableDefender defender) : base(defender)
    {
        weapons = defender.weapons.ForEach(x => x.name).ToList();
    }

    public new string Write()
    {
        return JsonConvert.SerializeObject(this);
    }
}
