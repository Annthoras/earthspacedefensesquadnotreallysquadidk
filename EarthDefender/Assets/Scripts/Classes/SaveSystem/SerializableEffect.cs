﻿//#define SelfMade
using System;
using System.Linq;
using System.Collections.Generic;
using Extensions;
using Newtonsoft.Json;
public class SerializableEffect : IReadable<SerializableEffect>, IWriteable
{
    public string name;
    public float duration;
    public int ticks;
    public bool isOffensive;
    public string spriteName;
    public bool isUnique;
    public List<EffectCompartment> compartments = new List<EffectCompartment>();
    public List<string> uniques = new List<string>();
    public SerializableEffect()
    {

    }
    public SerializableEffect(ScriptableEffect e)
    {
        this.name = e.name;
        this.duration = e.duration;
        this.ticks = e.ticks;
        this.isOffensive = e.isOffensive;
        this.spriteName = e.graphic.name;
        this.isUnique = e.isUnique;
        this.compartments = new List<EffectCompartment>(e.compartments);
        this.uniques = e.uniques.ForEach(x => x.name).ToList();
    }
    public SerializableEffect(string s)
    {
        Read(s);
    }
    public string Write()
    {
        return JsonConvert.SerializeObject(this);
#if SelfMade
        string result = name + ":\n{";
        result += "\n   Duration: " + duration.ToString() + ";";
        result += "\n   Ticks: " + ticks.ToString() + ";";
        result += "\n   Is Offensive: " + isOffensive + ";";
        result += "\n   Sprite: " + spriteName + ";";
        result += "\n   Is Unique: " + isUnique + ";";
        result += "\n   Compartments:";
        result += "\n   {";
        for (int i = 0; i < compartments.Count; i++)
        {
            result += "\n      Compartment " + i + ":";
            result += "\n      {";
            result += "\n         Affected Stat: " + compartments[i].affectedStat + ";";
            result += "\n         Calculation: " + compartments[i].calculation + ";";
            result += "\n         Target Used Stat: " + compartments[i].targetUsedStat + ";";
            result += "\n         Comparison Stat: " + compartments[i].comparisonStat + ";";
            result += "\n         Value: " + compartments[i].value + ";";
            result += "\n         Is Additive: " + compartments[i].isAdditive + ";";
            result += "\n      }";
        }
        result += "\n   }";
        result += "\n   Uniques:";
        result += "\n   {";
        for (int i = 0; i < uniques.Count; i++)
        {
            result += "\n      Unique 0:" + uniques[i] + ";";
        }
        result += "\n   }";
        result += "\n}";
        return result;
#endif
    }



    public void Read(string input)
    {
#if SelfMade
        name = "";
        for (int i = 0; i < input.Length; i++)
        {
            if (input[i] == ':')
                break;
            name += input[i];
        }
        name = FileManager.Trim(name);
        string valueOfObj = input.Substring(input.IndexOf('{') + 1).TrimAll('\n', ' ');
        int index = 0;
        duration = FileManager.Trim(FileManager.ParseObject("duration", valueOfObj, ref index)).ToFloat();
        ticks = FileManager.Trim(FileManager.ParseObject("ticks", valueOfObj, ref index)).ToInt();
        isOffensive = FileManager.Trim(FileManager.ParseObject("isoffensive", valueOfObj, ref index)).ToBool();
        spriteName = FileManager.Trim(FileManager.ParseObject("Sprite", valueOfObj, ref index));
        isUnique = FileManager.Trim(FileManager.ParseObject("isunique", valueOfObj, ref index)).ToBool();
        string compartments = FileManager.ParseObject("compartments", valueOfObj, ref index);
        int compIndex = 0;
        string currentcomp = FileManager.ParseObject("Compartment" + this.compartments.Count, compartments, ref compIndex);
        while (currentcomp != "NaN")
        {
            int compI = 0;
            EffectCompartment compartment = new EffectCompartment();
            Enum.TryParse(FileManager.Trim(FileManager.ParseObject("AffectedStat", currentcomp, ref compI)), out compartment.affectedStat);
            Enum.TryParse(FileManager.Trim(FileManager.ParseObject("Calculation", currentcomp, ref compI)), out compartment.calculation);
            Enum.TryParse(FileManager.Trim(FileManager.ParseObject("TargetUsedStat", currentcomp, ref compI)), out compartment.targetUsedStat);
            Enum.TryParse(FileManager.Trim(FileManager.ParseObject("ComparisonStat", currentcomp, ref compI)), out compartment.comparisonStat);
            compartment.value = FileManager.Trim(FileManager.ParseObject("value", currentcomp, ref compI)).ToFloat();
            compartment.isAdditive = FileManager.Trim(FileManager.ParseObject("isadditive", currentcomp, ref compI)).ToBool();
            this.compartments.Add(compartment);
            currentcomp = FileManager.ParseObject("Compartment" + this.compartments.Count, compartments, ref compIndex);
        }
        string uniques = FileManager.ParseObject("Uniques", valueOfObj, ref index);
        compIndex = 0;
        currentcomp = FileManager.ParseObject("Unique" + this.uniques.Count, uniques, ref compIndex);
        while (currentcomp != "NaN")
        {
            this.uniques.Add(currentcomp);
            currentcomp = FileManager.ParseObject("Unique" + this.uniques.Count, uniques, ref compIndex);
        }
#endif
    }





}