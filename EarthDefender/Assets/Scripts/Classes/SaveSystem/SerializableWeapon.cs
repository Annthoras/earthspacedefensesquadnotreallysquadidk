﻿using System;
using UnityEngine;
using Newtonsoft.Json;
using Extensions.Unity;
[Serializable]
public class SerializableWeapon : IReadable<SerializableWeapon>, IWriteable
{
    public float roundsPerSecond;
    public float projectileFlightSpeed;
    public int maxAmmo;
    public bool infiniteAmmo;
    public string usedProjectile;
    public float maxDegrees;
    public string sprite;
    public SerializableColor color;
    public bool keepColor;
    public float turnSpeed;
    public WeaponType type;
    public float weaponSpread;
    public int projectileAmmount;

    public SerializableWeapon() { }

    public SerializableWeapon(ScriptableWeapon w)
    {
        this.roundsPerSecond = w.roundsPerSecond;
        this.projectileFlightSpeed = w.projectileFlightSpeed;
        this.maxAmmo = w.maxAmmo;
        this.infiniteAmmo = w.infiniteAmmo;
        this.usedProjectile = w.usedProjectile.name;
        this.maxDegrees = w.maxDegrees;
        this.sprite = w.sprite.name;
        this.color = w.color;
        this.keepColor = w.keepColor;
        this.turnSpeed = w.turnSpeed;
        this.type = w.type;
        this.weaponSpread = w.weaponSpread;
        this.projectileAmmount = w.projectileAmmount;
    }


    public string Write()
    {
        return JsonConvert.SerializeObject(this);
    }
    public void Read(string data)
    {

    }
}