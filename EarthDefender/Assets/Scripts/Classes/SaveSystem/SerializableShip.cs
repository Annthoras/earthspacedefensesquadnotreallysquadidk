﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using Extensions;
using Newtonsoft.Json;
[Serializable]
public abstract class SerializableShip : IWriteable, IReadable<SerializableShip>
{
    public float baseHealth;
    public float baseShield;
    public float baseShieldRegen;
    public float baseShieldRegenDelay;
    public float baseArmor;
    public float baseSpeed;
    public float baseEnergy;
    public float baseFireRate = 1;
    public float turretTurnSpeed = 1;
    public string sprite;
    public Color color;
    public List<string> abilities = new List<string>();
    public List<string> maxProjectiles = new List<string>();
    public List<int> maxProjectilesAmmounts = new List<int>();
    public int faction;
    public string baseWeaponName;

    public SerializableShip() { }

    public SerializableShip(ScriptableShip scriptable)
    {
        this.baseHealth = scriptable.baseHealth;
        this.baseShield = scriptable.baseShield;
        this.baseShieldRegen = scriptable.baseShieldRegen;
        this.baseShieldRegenDelay = scriptable.baseShieldRegenDelay;
        this.baseArmor = scriptable.baseArmor;
        this.baseSpeed = scriptable.baseSpeed;
        this.baseEnergy = scriptable.baseEnergy;
        this.baseFireRate = scriptable.baseFireRate;
        this.turretTurnSpeed = scriptable.turretTurnSpeed;
        this.sprite = scriptable.sprite.name;
        this.color = scriptable.color;
        this.abilities = scriptable.abilities.ForEach(x => x.name).ToList();
        this.maxProjectiles = scriptable.maxProjectiles.ForEach(x => x.name).ToList();
        this.maxProjectilesAmmounts = new List<int>(scriptable.maxProjectilesAmmounts);
        this.faction = scriptable.Faction;
        this.baseWeaponName = scriptable.baseWeapon.name;
    }
    public string Write()
    {
        return JsonConvert.SerializeObject(this);
    }
    public void Read(string data)
    {

    }

    public float GetStat(Statistic stat)
    {
        switch (stat)
        {
            case Statistic.Health:
            case Statistic.BaseHealth:
            case Statistic.MaxHealth:
                return baseHealth;
            case Statistic.Speed:
            case Statistic.BaseSpeed:
                return baseSpeed;
            case Statistic.Energy:
            case Statistic.BaseEnergy:
            case Statistic.MaxEnergy:
                return baseEnergy;
            case Statistic.Shield:
            case Statistic.BaseShield:
            case Statistic.MaxShield:
                return baseShield;
            case Statistic.ShieldRegen:
            case Statistic.BaseShieldRegen:
                return baseShieldRegen;
            case Statistic.ShieldRegenDelay:
            case Statistic.BaseShieldRegenDelay:
                return baseShieldRegenDelay;
            case Statistic.Armor:
            case Statistic.BaseArmor:
            case Statistic.MaxArmor:
                return baseArmor;
            case Statistic.Firerate:
            case Statistic.BaseFirerate:
                return baseFireRate;
            case Statistic.Ammo:
            case Statistic.BaseAmmo:
            case Statistic.MaxAmmo:
                return 0;
            case Statistic.ProjectileSpeed:
                return 0;
            case Statistic.Damage:
                return 1;
            case Statistic.TurretTurnSpeed:
                return turretTurnSpeed;
            default:
                return 0;
        }
    }

}

