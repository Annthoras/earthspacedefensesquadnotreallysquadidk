﻿using System;
using System.Collections.Generic;
using System.Linq;
using Extensions;
using Newtonsoft.Json;
[Serializable]
public class SerializableInvader : SerializableShip, IReadable<SerializableInvader>
{
    public InvaderFirePattern firePattern;
    public string[] requiredNeighbors = new string[4];
    public int burstAmmount;
    public float burstWait;
    public int fireChance;
    public float pickupChance;
    public List<string> pickups = new List<string>();
    public List<float> pickupChances = new List<float>();
    public int score;

    public SerializableInvader() { }
    public SerializableInvader(ScriptableInvader inv) : base(inv)
    {
        this.firePattern = inv.firePattern;
        this.requiredNeighbors = inv.requiredNeighbors.ForEach(x => (x == null ? "" : x.name)).ToArray();
        this.burstAmmount = inv.burstAmmount;
        this.burstWait = inv.burstWait;
        this.fireChance = inv.fireChance;
        this.pickupChance = inv.pickupChance;
        this.pickups = inv.pickups.ForEach(x => x.name).ToList();
        this.pickupChances = new List<float>(inv.pickupChances);
        this.score = inv.score;
    }

    public new string Write()
    {
        return JsonConvert.SerializeObject(this);
    }
}