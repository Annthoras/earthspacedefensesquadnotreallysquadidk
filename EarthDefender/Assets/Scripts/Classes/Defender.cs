﻿//#define Debug
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Extensions.Unity;
using Extensions;
public class Defender : Ship
{
    public static Defender main { get; private set; }
    private float xAxisInput
    {
        get
        {
            return Input.GetAxisRaw("Horizontal");
        }
    }
    List<ScriptableWeapon> weapons;
#if Debug
    public List<Effect> effects;
#endif
    private void Awake()
    {
        Defender first = FindObjectOfType<Defender>();
        if (first != this && main != null && this != main)
            Destroy(this.gameObject);
        main = this;
        OnCreate();
    }
    public override void Init(SerializableShip reference)
    {
        if (reference.GetType() != typeof(CustomizedShip))
            throw new InvalidCastException("Invalid ScriptableShip type");
        this.Reference = reference;
        weapons = (this.Reference as CustomizedShip).weapons.ForEach(x => { return Resources.Load<ScriptableWeapon>(FileManager.ScriptableWeapons + x); }).ToList();
        weapons.Add(Resources.Load<ScriptableWeapon>(FileManager.ScriptableWeapons + (this.Reference as CustomizedShip).baseWeaponName));
        Faction = 0;
        base.Init(reference);
        GameManager.OnFixedUpdate += () => this.LookAt(UIManager.MouseRay.origin);
    }
    public void Update()
    {
#if Debug
        effects = activeEffects.ToList();
#endif

    }
    public static void SwitchWeapon(ScriptableWeapon wpn)
    {
        if (main.Reference.baseWeaponName == wpn.name || main.weapons.Contains(wpn))
        {
            main.CurrentWeapon = wpn;
            main.onStatChangeEvents[Statistic.MaxAmmo]();
        }
    }
    public static void UseAbility(ScriptableAbilitiy abil)
    {
        Ability match = main.abilities.FirstOrDefault(x => x.reference == abil);
        if (match != null && main.Energy > abil.energyCost)
            match.Activate();

    }
    public static void MoveMain(Vector2Int dir)
    {
        main.Move(dir);
    }
    public static void FireMain()
    {
        main.weapon.Fire();
        main.onStatChangeEvents[Statistic.Ammo]();
    }
    public override void LookAt(Vector3 target)
    {
        Vector3 dir = target - turret.transform.position;

        float angle = Mathf.Atan2(dir.x, dir.y) * Mathf.Rad2Deg;
        if (angle > CurrentWeapon.maxDegrees)
            angle = CurrentWeapon.maxDegrees;
        else if (angle < -CurrentWeapon.maxDegrees)
            angle = -CurrentWeapon.maxDegrees;
        Quaternion q = Quaternion.AngleAxis(-angle, Vector3.forward);
        turret.transform.rotation = Quaternion.Slerp(turret.rotation, q, this.TurretTurnSpeed * CurrentWeapon.turnSpeed * Time.deltaTime);
    }

    protected override void Die()
    {
        Destroy(this.gameObject);
        Debug.Log("Am ded");
    }
}

