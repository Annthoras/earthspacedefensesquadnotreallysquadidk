﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Extensions;
public class ShipSelector : DropdownSelector
{
    public List<ScriptableDefender> Ships { get; private set; }
    public Image img;
    public ColorPicker colorPicker;
    private bool changedColor = false;
    private Action onChangedColor;
    private CustomizedShip changedShip;

    protected override void Awake()
    {
        base.Awake();

        onValueChanged = () =>
        {
            img.sprite = Ships[CurrentIndex].sprite;
        };
        Ships = Resources.LoadAll<ScriptableDefender>("Scriptables/Ships/Defenders").ToList();
        Options = Ships.ForEach(x => x.name).ToList();
        CurrentIndex = 0;
        colorPicker.Init();
        onChangedColor = delegate { img.color = colorPicker.Colour; };
        colorPicker.OnColorChangedBind(() => onChangedColor());
        colorPicker.ChangeToColor(Ships[CurrentIndex].color);
    }

    private void Start()
    {
        onChangedColor = delegate
        {
            changedColor = true;
            img.color = colorPicker.Colour;
            onChangedColor = delegate { img.color = colorPicker.Colour; };
        };
    }
    public override void Next()
    {
        base.Next();
        changedShip = new CustomizedShip(Ships[CurrentIndex]);
        if (!changedColor)              
        {
            colorPicker.ChangeToColor(Ships[CurrentIndex].color, false);
            img.color = colorPicker.Colour;
        }
    }
}