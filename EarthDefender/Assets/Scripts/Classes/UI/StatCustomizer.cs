#define Test
#define Debug
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Extensions;

public class StatCustomizer : MonoBehaviour
{
    public Statistic statistic;
    public ShipSelector shipSelector;
    public TextMeshProUGUI itemText;
    public TextMeshProUGUI valueText;
    public Slider slider;
    private Action onDropdown;
    public int index;
    private void Start()
    {
        onDropdown = ActivateDropdown;
        index = this.transform.GetSiblingIndex();
        itemText.text = statistic.ToString().Replace("Base", "").Replace("Max", "");
        shipSelector.OnValueChangeBind(() => 
        {
            RefreshText();
        });
        RefreshText();
    }

    public void OnDropdown()
    {
        onDropdown();
    }
    private void Update()
    {
        index = this.transform.GetSiblingIndex();
    }
    private void ActivateDropdown()
    {
        slider.gameObject.SetActive(true);
        slider.transform.SetParent(this.transform.parent);
        slider.transform.SetSiblingIndex(index + 1);

        onDropdown = DeactivateDropdown;
    }
    private void DeactivateDropdown()
    {
        slider.gameObject.SetActive(false);
        slider.transform.SetParent(this.transform);
        onDropdown = ActivateDropdown;
    }

    public void RefreshText()
    {
        valueText.text = (shipSelector.Ships[shipSelector.CurrentIndex].GetStat(statistic) + slider.value).ToString();
    }
}