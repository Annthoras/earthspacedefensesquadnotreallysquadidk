﻿using System;
using UnityEngine;

[Serializable]
public class CustomizedShip : SerializableDefender
{
    public int healthChange;
    public int armorChange;
    public int energyChange;
    public int shieldChange;

    public CustomizedShip(ScriptableDefender d) : base(d)
    {

    }
}