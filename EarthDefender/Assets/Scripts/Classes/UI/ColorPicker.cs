﻿using System;
using UnityEngine;


public class ColorPicker : MonoBehaviour
{
    public InputFieldSlider red;
    public InputFieldSlider green;
    public InputFieldSlider blue;
    private Action onColorChange;

    public Color Colour { get; private set; }
    public void Init()
    {
        onColorChange = delegate { };
        onColorChange += () => Colour = new Color(red.Value / 255f, green.Value / 255f, blue.Value / 255f);
        red.Init();
        green.Init();
        blue.Init();
        red.OnValueChangedBind(() => onColorChange());
        green.OnValueChangedBind(() => onColorChange());
        blue.OnValueChangedBind(() => onColorChange());
    }


    public void OnColorChangedBind(Action act) => onColorChange += act;
    public void OnColorChangedUnBind(Action act) => onColorChange -= act;
    public void ChangeToColor(Color col, bool callback = true)
    {
        if (callback)
        {
            red.Value = col.r * 255f;
            green.Value = col.g * 255f;
            blue.Value = col.b * 255f;
        }
        else
        {
            this.Colour = col;
            red.SetWithoutNotify(col.r * 255f);
            green.SetWithoutNotify(col.g * 255f);
            blue.SetWithoutNotify(col.b * 255f);
        }
    }
}