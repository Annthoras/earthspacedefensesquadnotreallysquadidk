﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using Extensions;
using TMPro;
public class UIEffect : MonoBehaviour
{
    public Image Icon { get; private set; }
    public Image Overlay { get; private set; }
    public TextMeshProUGUI TimeText { get; private set; }
    public TextMeshProUGUI StackText { get; private set; }
    public ScriptableEffect effect;
    private bool coroutineRunning;
    public bool isWeaponEffect;
    private Effect currentWeaponEffect;
    private float weaponEffectDuration;
    private Action updateTime;
    private List<Effect> stacks;

    public void Init(ScriptableEffect effect, bool isWeaponEffect)
    {
        Icon = this.GetComponent<Image>();
        Overlay = this.transform.Find("Overlay").GetComponent<Image>();
        TimeText = this.transform.Find("Time").GetComponent<TextMeshProUGUI>();
        StackText = this.transform.Find("Stacks").GetComponent<TextMeshProUGUI>();
        this.effect = effect;
        this.isWeaponEffect = isWeaponEffect;
        stacks = new List<Effect>();
    }
    public void AddStack(Effect timeHandle)
    {
        if (this.effect.isUnique && stacks.Count > 0)
            stacks[0] = timeHandle;
        else
            stacks.Add(timeHandle);
        if (!coroutineRunning)
            StartCoroutine(GraphicDecay());
    }
    public void OverrideStack(Effect e, float duration)
    {
        currentWeaponEffect = e;
        weaponEffectDuration = duration;
        this.StackText.text = "";
        if (!coroutineRunning)
            StartCoroutine(WeaponGraphicDecay());
    }
    private IEnumerator WeaponGraphicDecay()
    {
        coroutineRunning = true;
        while (Defender.main.weapon.extraEffects[currentWeaponEffect] > 0)
        {
            Overlay.fillAmount = Defender.main.weapon.extraEffects[currentWeaponEffect] / weaponEffectDuration;
            TimeText.text = ((int)Defender.main.weapon.extraEffects[currentWeaponEffect]).ToString();
            yield return null;
        }
        GameUIManager.RemoveEffect(this);
    }
    private IEnumerator GraphicDecay()
    {
        coroutineRunning = true;
        while (stacks.Count > 0)
        {
            float longest = stacks.Max(x => x.SecondsLeft);
            Effect current = stacks.First(x => x.SecondsLeft == longest);
            Overlay.fillAmount = current.SecondsLeft / effect.duration;
            TimeText.text = ((int)current.SecondsLeft).ToString();
            stacks.RemoveWhere(x => x.SecondsLeft <= 0);
            StackText.text = stacks.Count > 1 ? "x" + stacks.Count : "";
            yield return null;
        }
        GameUIManager.RemoveEffect(this);
    }
}

