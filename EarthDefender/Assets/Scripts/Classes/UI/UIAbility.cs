﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class UIAbility : MonoBehaviour
{
    private Image icon;
    private Image coolDownOverlay;
    private TextMeshProUGUI coolDownText;
    private TextMeshProUGUI keybindText;
    private Ability target;


    public void Init(Ability abil)
    {
        icon = this.GetComponent<Image>();
        coolDownOverlay = this.transform.Find("CoolDownOverlay").GetComponent<Image>();
        coolDownText = this.transform.Find("CoolDownText").GetComponent<TextMeshProUGUI>();
        this.keybindText = this.transform.Find("KeybindText").GetComponent<TextMeshProUGUI>();
        target = abil;
        icon.sprite = target.reference.sprite;
        target.OnActivateBind(() => GameManager.OnUpdate += OnUpdate);
        target.OnCoolDownFinishBind(() => GameManager.OnUpdate -= OnUpdate);
        coolDownOverlay.fillAmount = 0;
        coolDownText.text = "";
    }
    public void OnUpdate()
    {
        coolDownOverlay.fillAmount = target.CurrentCooldown / target.CoolDown;
        coolDownText.text = (int)target.CurrentCooldown == 0 ? "" : ((int)target.CurrentCooldown).ToString();

    }
}

