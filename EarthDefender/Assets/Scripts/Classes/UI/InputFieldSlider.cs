using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Extensions;
public class InputFieldSlider : MonoBehaviour
{
    private float _value;
    [SerializeField]
    private bool _wholeNumbers;
    private TMP_InputField input;
    private Slider slider;
    [SerializeField]
    private float _minValue;
    [SerializeField]
    private float _maxValue;
    public float Value
    {
        get => _value;
        set
        {
            Set(value);
        }
    }

    public bool WholeNumbers
    {
        get => _wholeNumbers;
        set
        {
            if (value)
            {
                MinValue = Mathf.RoundToInt(MinValue);
                MaxValue = Mathf.RoundToInt(MaxValue);
            }
            _wholeNumbers = value;
            slider.wholeNumbers = _wholeNumbers;
        }
    }
    public float MinValue
    {
        get => _minValue;
        set
        {
            _minValue = value;
            if (Value < _minValue)
                Value = _minValue;
        }
    }
    public float MaxValue
    {
        get => _maxValue;
        set
        {
            _maxValue = value;
            if (Value > _maxValue)
                Value = _maxValue;
        }
    }
    private Action onValueChanged = delegate { };

    public void Init()
    {
        input = this.GetComponentInChildren<TMP_InputField>();
        slider = this.GetComponentInChildren<Slider>();
        WholeNumbers = WholeNumbers;
        MinValue = MinValue;
        MaxValue = MaxValue;
        slider.maxValue = MaxValue;
        slider.minValue = MinValue;
    }
    public void ChangeValue(string newVal)
    {
        Value = newVal.ToFloat();
    }
    public void ChangeValue(float newVal)
    {
        Value = newVal;
    }

    public void OnValueChangedBind(Action act) => onValueChanged += act;
    public void OnValueChangeUnbind(Action act) => onValueChanged -= act;

    private void Set(float value, bool callBack = true)
    {
        _value = value;
        if (_value < MinValue)
            _value = MinValue;
        else if (_value > MaxValue)
            _value = MaxValue;
        if (WholeNumbers)
            _value = Mathf.RoundToInt(_value);
        slider.SetValueWithoutNotify(_value);
        input.SetTextWithoutNotify(_value.ToString());
        if (callBack)
            onValueChanged();

    }
    public void SetWithoutNotify(float value)
    {
        Set(value, false);
    }

}