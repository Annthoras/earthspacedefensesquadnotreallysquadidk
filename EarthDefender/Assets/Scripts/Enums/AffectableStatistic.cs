﻿
public enum AffectableStatistic
{
    Health = 0,
    Armor = 1,
    Firerate = 2,
    Speed=3,
    Energy = 4,
    Shield = 5,
    ShieldRegen = 6,
    ShieldRegenDelay = 7,
    MaxHealth,
    MaxEnergy,
    MaxShield,
    MaxArmor,
    MaxAmmo,
    Damage,
    TurretTurnSpeed,
    Ammo
}

