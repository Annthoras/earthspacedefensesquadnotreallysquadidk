﻿public enum InvaderFirePattern
{
    Continuous,
    Burst,
    Random
}