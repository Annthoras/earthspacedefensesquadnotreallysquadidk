﻿public enum AbilityType
{
    None,
    Bubble,
    ExtraWeapon,
    AreaOfEffect,
    Effect,
}

