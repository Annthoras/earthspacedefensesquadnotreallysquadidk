﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public enum DamageType
{
    Absolute,
    Piercing,
    Impact,
    Fire,
    Explosive,
    Frost,
    Electric,
    Corrosive,
    Magnetic
}

