﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Extensions.Unity
{
    public static class Vector3Extension
    {
        public static Vector3[] Cardinals
        {
            get
            {
                return new Vector3[]
                {
                    new Vector3(1,0,0),
                    new Vector3(-1,0,0),
                    new Vector3(0,1,0),
                    new Vector3(0,-1,0),
                    new Vector3(0,0,1),
                    new Vector3(0,0,-1),
                };
            }
        }
        public static Vector3[] Diagonals
        {
            get
            {
                return new Vector3[]
                {
                    new Vector3(1,0,0),
                    new Vector3(-1,0,0),
                    new Vector3(0,1,0),
                    new Vector3(0,-1,0),
                    new Vector3(0,0,1),
                    new Vector3(0,0,-1),
                    new Vector3(1,1,0),
                    new Vector3(-1,1,0),
                    new Vector3(1,-1,0),
                    new Vector3(-1,-1,0),
                    new Vector3(0,1,1),
                    new Vector3(0,1,-1),
                    new Vector3(0,-1,1),
                    new Vector3(0,-1,-1),
                    new Vector3(1,0,1),
                    new Vector3(-1,0,1),
                    new Vector3(1,0,-1),
                    new Vector3(-1,0,-1),
                    new Vector3(1,1,1),
                    new Vector3(-1,1,1),
                    new Vector3(1,-1,1),
                    new Vector3(-1,-1,1),
                    new Vector3(1,1,-1),
                    new Vector3(-1,1,-1),
                    new Vector3(1,-1,-1),
                    new Vector3(-1,-1,-1),
                };
            }
        }
        public static bool InRange(this Vector3 v3, Vector3 other, float range)
        {
            return Vector3.Distance(v3, other) <= range;
        }
        public static bool WithinBounds(this Vector3 v3, Vector3 min, Vector3 max)
        {
            return (v3.x >= min.x && v3.x <= max.x) && (v3.y >= min.y && v3.y <= max.y) && (v3.z >= min.z && v3.z <= max.z);
        }
        public static bool OutOfBounds(this Vector3 v3, Vector3 min, Vector3 max)
        {
            return (v3.x < min.x || v3.x > max.x) || (v3.y < min.y || v3.y > max.y) || (v3.z < min.z || v3.z > max.z);
        }
        public static Vector3[] LocalCardinals(this Vector3 v3)
        {
            return new Vector3[]
            {
                v3 + new Vector3(1,0,0),
                v3 + new Vector3(-1,0,0),
                v3 + new Vector3(0,1,0),
                v3 + new Vector3(0,-1,0),
                v3 + new Vector3(0,0,1),
                v3 + new Vector3(0,0,-1),

            };
        }
        public static Vector3[] LocalDiagonals(this Vector3 v3)
        {
            return new Vector3[]
            {
                    v3 + new Vector3(1,0,0),
                    v3 + new Vector3(-1,0,0),
                    v3 + new Vector3(0,1,0),
                    v3 + new Vector3(0,-1,0),
                    v3 + new Vector3(0,0,1),
                    v3 + new Vector3(0,0,-1),
                    v3 + new Vector3(1,1,0),
                    v3 + new Vector3(-1,1,0),
                    v3 + new Vector3(1,-1,0),
                    v3 + new Vector3(-1,-1,0),
                    v3 + new Vector3(0,1,1),
                    v3 + new Vector3(0,1,-1),
                    v3 + new Vector3(0,-1,1),
                    v3 + new Vector3(0,-1,-1),
                    v3 + new Vector3(1,0,1),
                    v3 + new Vector3(-1,0,1),
                    v3 + new Vector3(1,0,-1),
                    v3 + new Vector3(-1,0,-1),
                    v3 + new Vector3(1,1,1),
                    v3 + new Vector3(-1,1,1),
                    v3 + new Vector3(1,-1,1),
                    v3 + new Vector3(-1,-1,1),
                    v3 + new Vector3(1,1,-1),
                    v3 + new Vector3(-1,1,-1),
                    v3 + new Vector3(1,-1,-1),
                    v3 + new Vector3(-1,-1,-1),
            };
        }
        public static Vector2 ToVector2Y(this Vector3 v3)
        {
            return new Vector2(v3.x, v3.y);
        }
        public static Vector2 ToVector2Z(this Vector3 v3)
        {
            return new Vector2(v3.x, v3.z);
        }
    }
    public static class Vector3IntExtension
    {
        public static Vector3Int[] Cardinals
        {
            get
            {
                return new Vector3Int[]
                {
                    new Vector3Int(1,0,0),
                    new Vector3Int(-1,0,0),
                    new Vector3Int(0,1,0),
                    new Vector3Int(0,-1,0),
                    new Vector3Int(0,0,1),
                    new Vector3Int(0,0,-1),
                };
            }
        }
        public static Vector3Int[] Diagonals
        {
            get
            {
                return new Vector3Int[]
                {
                    new Vector3Int(1,0,0),
                    new Vector3Int(-1,0,0),
                    new Vector3Int(0,1,0),
                    new Vector3Int(0,-1,0),
                    new Vector3Int(0,0,1),
                    new Vector3Int(0,0,-1),
                    new Vector3Int(1,1,0),
                    new Vector3Int(-1,1,0),
                    new Vector3Int(1,-1,0),
                    new Vector3Int(-1,-1,0),
                    new Vector3Int(0,1,1),
                    new Vector3Int(0,1,-1),
                    new Vector3Int(0,-1,1),
                    new Vector3Int(0,-1,-1),
                    new Vector3Int(1,0,1),
                    new Vector3Int(-1,0,1),
                    new Vector3Int(1,0,-1),
                    new Vector3Int(-1,0,-1),
                    new Vector3Int(1,1,1),
                    new Vector3Int(-1,1,1),
                    new Vector3Int(1,-1,1),
                    new Vector3Int(-1,-1,1),
                    new Vector3Int(1,1,-1),
                    new Vector3Int(-1,1,-1),
                    new Vector3Int(1,-1,-1),
                    new Vector3Int(-1,-1,-1),
                };
            }
        }
        public static bool InRange(this Vector3Int v3, Vector3Int other, float range)
        {
            return Vector3Int.Distance(v3, other) <= range;
        }
        public static bool WithinBounds(this Vector3Int v3, Vector3Int min, Vector3Int max)
        {
            return (v3.x >= min.x && v3.x <= max.x) && (v3.y >= min.y && v3.y <= max.y) && (v3.z >= min.z && v3.z <= max.z);
        }
        public static bool OutOfBounds(this Vector3Int v3, Vector3Int min, Vector3Int max)
        {
            return (v3.x < min.x || v3.x > max.x) || (v3.y < min.y || v3.y > max.y) || (v3.z < min.z || v3.z > max.z);
        }
        public static Vector3Int[] LocalCardinals(this Vector3Int v3)
        {
            return new Vector3Int[]
            {
                v3 + new Vector3Int(1,0,0),
                v3 + new Vector3Int(-1,0,0),
                v3 + new Vector3Int(0,1,0),
                v3 + new Vector3Int(0,-1,0),
                v3 + new Vector3Int(0,0,1),
                v3 + new Vector3Int(0,0,-1),

            };
        }
        public static Vector3Int[] LocalDiagonals(this Vector3Int v3)
        {
            return new Vector3Int[]
            {
                    v3 + new Vector3Int(1,0,0),
                    v3 + new Vector3Int(-1,0,0),
                    v3 + new Vector3Int(0,1,0),
                    v3 + new Vector3Int(0,-1,0),
                    v3 + new Vector3Int(0,0,1),
                    v3 + new Vector3Int(0,0,-1),
                    v3 + new Vector3Int(1,1,0),
                    v3 + new Vector3Int(-1,1,0),
                    v3 + new Vector3Int(1,-1,0),
                    v3 + new Vector3Int(-1,-1,0),
                    v3 + new Vector3Int(0,1,1),
                    v3 + new Vector3Int(0,1,-1),
                    v3 + new Vector3Int(0,-1,1),
                    v3 + new Vector3Int(0,-1,-1),
                    v3 + new Vector3Int(1,0,1),
                    v3 + new Vector3Int(-1,0,1),
                    v3 + new Vector3Int(1,0,-1),
                    v3 + new Vector3Int(-1,0,-1),
                    v3 + new Vector3Int(1,1,1),
                    v3 + new Vector3Int(-1,1,1),
                    v3 + new Vector3Int(1,-1,1),
                    v3 + new Vector3Int(-1,-1,1),
                    v3 + new Vector3Int(1,1,-1),
                    v3 + new Vector3Int(-1,1,-1),
                    v3 + new Vector3Int(1,-1,-1),
                    v3 + new Vector3Int(-1,-1,-1),
            };
        }
        public static Vector2Int ToVector2IntY(this Vector3Int v3)
        {
            return new Vector2Int(v3.x, v3.y);
        }
        public static Vector2Int ToVector2IntZ(this Vector3Int v3)
        {
            return new Vector2Int(v3.x, v3.z);
        }
    }
    public static class Vector2Extension
    {
        public static Vector2[] Cardinals
        {
            get
            {
                return new Vector2[]
                {
                    Vector2.up,
                    -Vector2.up,
                    Vector2.right,
                    -Vector2.right
                };
            }
        }
        public static Vector2[] Diagonals
        {
            get
            {
                return new Vector2[]
                {
                    Vector2.up,
                    -Vector2.up,
                    Vector2.right,
                    -Vector2.right,
                    new Vector2(1,1),
                    new Vector2(1,-1),
                    new Vector2(-1,1),
                    new Vector2(-1,-1)
                };
            }
        }
        public static bool InRange(this Vector2 v2, Vector2 other, float range)
        {
            return Vector2.Distance(v2, other) <= range;
        }
        public static bool WithinBounds(this Vector2 v2, Vector2 min, Vector2 max)
        {
            return (v2.x >= min.x && v2.x <= max.x) && (v2.y >= min.y && v2.y <= max.y);
        }
        public static bool OutOfBounds(this Vector2 v2, Vector2 min, Vector2 max)
        {
            return (v2.x < min.x || v2.x > max.x) || (v2.y < min.y || v2.y > max.y);
        }
        public static Vector2[] LocalCardinals(this Vector2 v2)
        {
            return new Vector2[]
            {
                v2 + new Vector2(1,0),
                v2 + new Vector2(0,1),
                v2 + new Vector2(-1,0),
                v2 + new Vector2(0,-1),
            };
        }
        public static Vector2[] LocalDiagonals(this Vector2 v2)
        {
            return new Vector2[]
            {
                v2 + new Vector2(1,0),
                v2 + new Vector2(0,1),
                v2 + new Vector2(-1,0),
                v2 + new Vector2(0,-1),
                v2 + new Vector2(1,1),
                v2 + new Vector2(1,-1),
                v2 + new Vector2(-1,1),
                v2 + new Vector2(-1,-1),
            };
        }
        public static Vector3 ToVector3Y(this Vector2 v2)
        {
            return new Vector3(v2.x, v2.y, 0);
        }
        public static Vector3 ToVector3Z(this Vector2 v2)
        {
            return new Vector3(v2.x, 0, v2.y);
        }

    }
    public static class Vector2IntExtension
    {
        public static Vector2Int[] Cardinals
        {
            get
            {
                return new Vector2Int[]
                {
                    Vector2Int.up,
                    new Vector2Int(0,-1),
                    Vector2Int.right,
                    new Vector2Int(-1,0)
                };
            }
        }
        public static Vector2Int[] Diagonals
        {
            get
            {
                return new Vector2Int[]
                {
                    Vector2Int.up,
                    new Vector2Int(0,-1),
                    Vector2Int.right,
                    new Vector2Int(-1,0),
                    new Vector2Int(1,1),
                    new Vector2Int(1,-1),
                    new Vector2Int(-1,1),
                    new Vector2Int(-1,-1)
                };
            }
        }
        public static bool InRange(this Vector2Int v2, Vector2Int other, float range)
        {
            return Vector2Int.Distance(v2, other) <= range;
        }
        public static bool WithinBounds(this Vector2Int v2, Vector2Int min, Vector2Int max)
        {
            return (v2.x >= min.x && v2.x <= max.x) && (v2.y >= min.y && v2.y <= max.y);
        }
        public static bool OutOfBounds(this Vector2Int v2, Vector2Int min, Vector2Int max)
        {
            return (v2.x < min.x || v2.x > max.x) || (v2.y < min.y || v2.y > max.y);
        }
        public static Vector2Int[] LocalCardinals(this Vector2Int v2)
        {
            return new Vector2Int[]
            {
                v2 + new Vector2Int(1,0),
                v2 + new Vector2Int(0,1),
                v2 + new Vector2Int(-1,0),
                v2 + new Vector2Int(0,-1),
            };
        }
        public static Vector2Int[] LocalDiagonals(this Vector2Int v2)
        {
            return new Vector2Int[]
            {
                v2 + new Vector2Int(1,0),
                v2 + new Vector2Int(0,1),
                v2 + new Vector2Int(-1,0),
                v2 + new Vector2Int(0,-1),
                v2 + new Vector2Int(1,1),
                v2 + new Vector2Int(1,-1),
                v2 + new Vector2Int(-1,1),
                v2 + new Vector2Int(-1,-1),
            };
        }
        public static Vector3Int ToVector3IntY(this Vector2Int v2)
        {
            return new Vector3Int(v2.x, v2.y, 0);
        }
        public static Vector3Int ToVector3IntZ(this Vector2Int v2)
        {
            return new Vector3Int(v2.x, 0, v2.y);
        }
        public static Vector2 ToVector2(this Vector2Int v2) => new Vector2(v2.x, v2.y);

    }


    [Serializable]
    public struct SerializableVector3
    {
        public float x, y, z;

        public SerializableVector3(float x, float y) { this.x = x; this.y = y; this.z = 0; }
        public SerializableVector3(float x, float y, float z) { this.x = x; this.y = y; this.z = z; }
        public SerializableVector3(Vector3 v3) { this.x = v3.x; this.y = v3.y; this.z = v3.z; }

        public static implicit operator Vector3(SerializableVector3 sV3) { return new Vector3(sV3.x, sV3.y, sV3.z); }
        public static implicit operator SerializableVector3(Vector3 v3) { return new SerializableVector3(v3.x, v3.y, v3.z); }
    }
    [Serializable]
    public struct SerializableVector3Int
    {
        public int x, y, z;

        public SerializableVector3Int(int x, int y) { this.x = x; this.y = y; this.z = 0; }
        public SerializableVector3Int(int x, int y, int z) { this.x = x; this.y = y; this.z = z; }
        public SerializableVector3Int(Vector3Int v3) { this.x = v3.x; this.y = v3.y; this.z = v3.z; }

        public static implicit operator Vector3Int(SerializableVector3Int sV3) { return new Vector3Int(sV3.x, sV3.y, sV3.z); }
        public static implicit operator SerializableVector3Int(Vector3Int v3) { return new SerializableVector3Int(v3.x, v3.y, v3.z); }
    }
    [Serializable]
    public struct SerializableVector2
    {
        public float x, y;
        public SerializableVector2(float x, float y) { this.x = x; this.y = y; }
        public SerializableVector2(Vector2 v2) { this.x = v2.x; this.y = v2.y; }
        public static implicit operator Vector2(SerializableVector2 sV2) { return new Vector2(sV2.x, sV2.y); }
        public static implicit operator SerializableVector2(Vector2 v2) { return new SerializableVector2(v2); }
    }
    [Serializable]
    public struct SerializableVector2Int
    {
        public int x, y;
        public SerializableVector2Int(int x, int y) { this.x = x; this.y = y; }
        public SerializableVector2Int(Vector2Int v2) { this.x = v2.x; this.y = v2.y; }
        public static implicit operator Vector2Int(SerializableVector2Int sV2) { return new Vector2Int(sV2.x, sV2.y); }
        public static implicit operator SerializableVector2Int(Vector2Int v2) { return new SerializableVector2Int(v2); }
    }

    [Serializable]
    public struct SerializableColor
    {
        //
        // Summary:
        //     Red component of the color.
        public float r;
        //
        // Summary:
        //     Green component of the color.
        public float g;
        //
        // Summary:
        //     Blue component of the color.
        public float b;
        //
        // Summary:
        //     Alpha component of the color (0 is transparent, 1 is opaque).
        public float a;
        public SerializableColor(float r, float g, float b, float a)
        {
            this.r = r;
            this.g = g;
            this.b = b;
            this.a = a;
        }
        public SerializableColor(float r, float g, float b) : this(r, g, b, 1) { }
        public SerializableColor(Color c) : this(c.r, c.g, c.b, c.a) { }

        public static implicit operator Color(SerializableColor c)=>new Color(c.r, c.g, c.b, c.a);
        public static implicit operator SerializableColor(Color c) => new SerializableColor(c);
    }
}
