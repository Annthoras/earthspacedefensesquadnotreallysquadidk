﻿#if UNITY_EDITOR
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;
[CustomEditor(typeof(ScriptableWeapon))]
public class WeaponEditor : Editor
{
    private ScriptableWeapon targetWeapon;

    private void OnEnable() => targetWeapon = (ScriptableWeapon)target;

    public override void OnInspectorGUI()
    {
        EditorGUI.BeginChangeCheck();
        EditorGUILayout.BeginVertical();
        if (GUILayout.Button("Save To File"))
            FileManager.WriteToFile(targetWeapon.name, new SerializableWeapon(targetWeapon), "wpn");
        if (GUILayout.Button("Load From File"))
            targetWeapon.FromSerialized(FileManager.ReadFromFile<SerializableWeapon>(targetWeapon.name + ".wpn"));
        targetWeapon.roundsPerSecond = EditorGUILayout.FloatField(new GUIContent("Rounds per second", "The ammount of projectiles the weapons fires per second"), targetWeapon.roundsPerSecond);
        targetWeapon.projectileFlightSpeed = EditorGUILayout.FloatField(new GUIContent("Projectile flight speed", "The flight speed multiplier of the weapon"), targetWeapon.projectileFlightSpeed);
        targetWeapon.infiniteAmmo = EditorGUILayout.Toggle(new GUIContent("Infinite Ammo", "Does the weapon have infinite ammo?"), targetWeapon.infiniteAmmo);
        if (!targetWeapon.infiniteAmmo)
        {
            targetWeapon.maxAmmo = EditorGUILayout.IntField(new GUIContent("Max Ammo", "The baseline maximum ammount of ammo the weapon has"), targetWeapon.maxAmmo);
            if (targetWeapon.maxAmmo < 1)
                targetWeapon.maxAmmo = 1;
        }
        targetWeapon.usedProjectile = (ScriptableProjectile)EditorGUILayout.ObjectField(new GUIContent("Projectile", "The projectile the weapon uses"), targetWeapon.usedProjectile, typeof(ScriptableProjectile), false);
        targetWeapon.maxDegrees = EditorGUILayout.FloatField(new GUIContent("Max Degrees", "The maximum ammount of degrees the weapon can turn in a direction"), targetWeapon.maxDegrees);
        targetWeapon.turnSpeed = EditorGUILayout.FloatField(new GUIContent("Turn Speed", "How fast the weapon turns to point at it's target"), targetWeapon.turnSpeed);
        targetWeapon.type = (WeaponType)EditorGUILayout.EnumPopup(new GUIContent("Type", "The type of weapon"), targetWeapon.type);
        if (targetWeapon.type == WeaponType.ShotgunLike)
        {
            targetWeapon.weaponSpread = EditorGUILayout.FloatField(new GUIContent("Spread", "The spread of angle of the shotgun"), targetWeapon.weaponSpread);
            targetWeapon.projectileAmmount = EditorGUILayout.IntField(new GUIContent("Projectile ammount", "The ammount of projectiles fired with each shot"), targetWeapon.projectileAmmount);
        }
        targetWeapon.sprite = (Sprite)EditorGUILayout.ObjectField(new GUIContent("Sprite", "The weapon's appearance"), targetWeapon.sprite, typeof(Sprite), false);
        targetWeapon.keepColor = EditorGUILayout.Toggle(new GUIContent("Keep Color", "Should the weapon keep it's original color?"), targetWeapon.keepColor);
        if (targetWeapon.keepColor)
            targetWeapon.color = EditorGUILayout.ColorField(new GUIContent("Color", "The color of the weapon"), targetWeapon.color);
        EditorGUILayout.EndVertical();
        if (EditorGUI.EndChangeCheck())
            EditorUtility.SetDirty(targetWeapon);
    }

}

#endif