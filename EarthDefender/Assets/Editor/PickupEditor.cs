﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using Extensions.Unity.Editor;
[CustomEditor(typeof(ScriptablePickup))]
public class PickupEditor : Editor
{
    ScriptablePickup targetPickup;
    private bool showEffects;
    public void OnEnable()
    {
        targetPickup = (ScriptablePickup)target;
        showEffects = false;
    }
    public override void OnInspectorGUI()
    {

        EditorGUI.BeginChangeCheck();
        EditorGUILayout.BeginVertical();
        targetPickup.sprite = (Sprite)EditorGUILayout.ObjectField("Sprite", targetPickup.sprite, typeof(Sprite), false);
        targetPickup.effects = new List<ScriptableEffect>(this.SaveCollection("Effects", "Effect", targetPickup.effects, ref showEffects, r =>
          {
              return (ScriptableEffect)EditorGUILayout.ObjectField(r, typeof(ScriptableEffect), false);
          }));
        EditorGUILayout.EndVertical();
        if (EditorGUI.EndChangeCheck())
            EditorUtility.SetDirty(targetPickup);
    }
}

