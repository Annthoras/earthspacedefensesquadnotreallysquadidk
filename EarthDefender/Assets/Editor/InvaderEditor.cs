﻿// #define Test
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;
using Extensions;
using Extensions.Unity.Editor;
[CustomEditor(typeof(ScriptableInvader))]
public class InvaderEditor : ShipEditor
{
    protected ScriptableInvader targetInvader;
    private bool showRequiredNeighbors;
    private bool showPickups;
    protected override void OnEnable()
    {
        base.OnEnable();
        targetInvader = (ScriptableInvader)targetShip;
        showRequiredNeighbors = false;
        showPickups = false;
    }
    public override void OnInspectorGUI()
    {
        EditorGUI.BeginChangeCheck();
        EditorGUILayout.BeginVertical();
        if (GUILayout.Button("Save To File"))
            FileManager.WriteToFile(targetInvader.name, new SerializableInvader(targetInvader), "inv");
        if (GUILayout.Button("Load From File"))
            targetInvader.FromSerialized(FileManager.ReadFromFile<SerializableInvader>(targetInvader.name + ".inv"));
        ShowSharedValues();
        EditorGUILayout.Space();
        targetInvader.score = EditorGUILayout.IntField("Score" , targetInvader.score);
        targetInvader.firePattern = (InvaderFirePattern)EditorGUILayout.EnumPopup(new GUIContent("Fire Pattern", "The fire pattern of the invader"), targetInvader.firePattern);
        if (targetInvader.firePattern == InvaderFirePattern.Burst)
        {
            targetInvader.burstAmmount = EditorGUILayout.IntField(new GUIContent("Burst Ammount", "The number of bullets per burst"), targetInvader.burstAmmount);
            if (targetInvader.burstAmmount < 0)
                targetInvader.burstAmmount = 0;
            targetInvader.burstWait = EditorGUILayout.FloatField(new GUIContent("Burst Wait", "Time between each burst in seconds"), targetInvader.burstWait);
            if (targetInvader.burstWait < 0.1f)
                targetInvader.burstWait = 0.1f;
        }
        else if (targetInvader.firePattern == InvaderFirePattern.Random)
        {
            targetInvader.fireChance = EditorGUILayout.IntField(new GUIContent("Fire Chance", "The chance of the invader firing"), targetInvader.fireChance);
            if (targetInvader.fireChance > 100)
                targetInvader.fireChance = 100;
            else if (targetInvader.fireChance < 0)
                targetInvader.fireChance = 0;
        }
        showRequiredNeighbors = EditorGUILayout.Foldout(showRequiredNeighbors, new GUIContent("Required Neighbors", "The Invaders that must spawn next to this one. \n0 => up, 1 => right, 2 => left, 3 => down"));
        if (showRequiredNeighbors)
        {
            for (int i = 0; i < 4; i++)
                targetInvader.requiredNeighbors[i] = (ScriptableInvader)EditorGUILayout.ObjectField("Neighbor " + i, targetInvader.requiredNeighbors[i], typeof(ScriptableInvader), false);
        }
        EditorGUILayout.Space();
        targetInvader.pickupChance = EditorGUILayout.Slider(new GUIContent("Pickup Chance", "The chance of this Invader dropping a Pickup on death"), targetInvader.pickupChance, 0f, 1f);
        //This whole if statement is a shitshow
        //will fix it... at some point
        if (targetInvader.pickupChance > 0f)
        {
            List<ScriptablePickup> resultA = new List<ScriptablePickup>(targetInvader.pickups);
            List<decimal> resultB = new List<decimal>(targetInvader.pickupChances.ForEach(x => (decimal)x));
            EditorGUILayout.BeginHorizontal();
            showPickups = EditorGUILayout.Foldout(showPickups, "Pickups", true);
            EditorGUILayout.Space(5);
            if (resultA.Count != resultB.Count)
            {
                Debug.LogWarning("Collections are not of the same length, creating new item in the smaller collection");
                if (resultA.Count > resultB.Count)
                    for (int i = resultB.Count; i < resultA.Count; i++)
                        resultB.Add(0m);
                else
                    for (int i = resultA.Count; i < resultB.Count; i++)
                        resultA.Add(null);
            }
            int length = resultA.Count;
            length = EditorGUILayout.IntField("Length", length);
            if (GUILayout.Button("+"))
                length++;
            if (GUILayout.Button("-"))
                length--;
            if (length > resultA.Count)
                for (int i = resultA.Count; i < length; i++)
                {
                    resultA.Add(null);
                    if (resultB.Count == 0)
                        resultB.Add(1m);
                    else
                        resultB.Add(0m);
                }
            else if (length < resultA.Count)
            {
                List<ScriptablePickup> newResultA = new List<ScriptablePickup>();
                List<decimal> newResultB = new List<decimal>();
                for (int i = 0; i < length; i++)
                {
                    newResultA.Add(resultA[i]);
                    newResultB.Add(resultB[i]);
                }
                resultA = newResultA;
                resultB = newResultB;
            }


            else EditorGUILayout.LabelField(resultA.Count.ToString());
            EditorGUILayout.EndHorizontal();
            if (showPickups)
            {
                length = resultA.Count;
                for (int i = 0; i < length; i++)
                {
                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.Space(-500);
                    resultA[i] = (ScriptablePickup)EditorGUILayout.ObjectField(resultA[i], typeof(ScriptablePickup), false, GUILayout.Width(150f));
                    EditorGUILayout.Space(-500);
                    decimal result = resultB[i];
                    result = (decimal)EditorGUILayout.FloatField((float)result);
                    if (result > 1)
                        result = 1;
                    else if (result < 0)
                        result = 0;
#if Test
                    result = (decimal)EditorGUILayout.Slider((float)resultB[i], 0, 1f, GUILayout.Width(200f));
                    //check if value changed
                    if (result != resultB[i])
                    {
                        //if so, go through the whole list
                        for (int x = 0; x < resultB.Count; x++)
                        {
                            //for all of the values that are not the one the user just changed
                            if (i != x)
                            {
                                //if result is one, all other values should be 0
                                if (result == 1)
                                    resultB[x] = 0;
                                else
                                {
                                    List<decimal> resultBNoI = new List<decimal>(resultB);
                                    resultBNoI.RemoveAt(i);
                                    int countWithoutZeroes = resultBNoI.Where(r => r != 0).Count();
                                    //take the new value, subtract the old one to get the difference
                                    //divide the difference by the number of OTHER values in the list
                                    //and then subtract all of that from each of the other values
                                    resultB[x] -= (result - resultB[i]) / (countWithoutZeroes - 1 <= 1 ? 1 : countWithoutZeroes - 1);
                                    //check if it's going to go under zero (not allowed)
                                    if (resultB[x] <= 0)
                                        resultB[x] = 0;
                                    //check if it's going to go over one (not allowed)
                                    if (resultB[x] >= 1)
                                        resultB[x] = 1;
                                    //attempt at fixing a bug where if you messed with values long enough
                                    //some values would begin to lose their proportionality
                                    decimal sumWithoutX = 0m;
                                    for (int y = 0; y < resultB.Count; y++)
                                    {
                                        if (x != y)
                                            sumWithoutX += resultB[y];

                                    }
                                    if (resultB[x] > 1 - sumWithoutX)
                                        resultB[x] = 1 - sumWithoutX;
                                }
                            }
                        }

                    }
#endif
                    EditorGUILayout.EndHorizontal();
                    EditorGUILayout.Space();
                    resultB[i] = result;
                }
            }
            targetInvader.pickups = resultA;
            targetInvader.pickupChances = resultB.ForEach(x => (float)x).ToList();
        }
        EditorGUILayout.EndVertical();
        if (EditorGUI.EndChangeCheck())
            EditorUtility.SetDirty(targetShip);
    }
}

