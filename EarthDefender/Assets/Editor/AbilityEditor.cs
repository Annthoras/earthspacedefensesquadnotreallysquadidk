﻿#define Test
#if UNITY_EDITOR
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using Extensions.Unity.Editor;
[CustomEditor(typeof(ScriptableAbilitiy))]
public class AbilityEditor : Editor
{
    private ScriptableAbilitiy targetAbility;
    private Action layout = delegate { };
    private AbilityType previousType;
    private bool showEffects;
    private List<bool> showDelays;

    private void OnEnable()
    {
        targetAbility = (ScriptableAbilitiy)target;
        previousType = AbilityType.None;
        showDelays = new List<bool>();
        for (int i = 0; i < targetAbility.effects.Count; i++)
        {
            showDelays.Add(false);
        }
    }

    public override void OnInspectorGUI()
    {
        EditorGUI.BeginChangeCheck();
        EditorGUILayout.BeginVertical();
        targetAbility.type = (AbilityType)EditorGUILayout.EnumPopup(new GUIContent("Type", "Ability Type"), targetAbility.type);
        if (targetAbility.type == AbilityType.None)
        {
            if (previousType == AbilityType.None)
                targetAbility.type = AbilityType.Bubble;
            else targetAbility.type = previousType;
        }
        targetAbility.energyCost = EditorGUILayout.IntField(new GUIContent("Cost", "The amount of energy it will cost to use the ability"), targetAbility.energyCost);
        targetAbility.coolDown = EditorGUILayout.FloatField(new GUIContent("Cooldown", "How long (in s) until the ability can be used again"), targetAbility.coolDown);
        if (targetAbility.effects.Any(x => x != null && x.isOffensive))
            targetAbility.duration = EditorGUILayout.FloatField(new GUIContent("Duration", "How long (in s) offensive effects will be applied to the weapon for"), targetAbility.duration);
        targetAbility.sprite = (Sprite)EditorGUILayout.ObjectField("Sprite", targetAbility.sprite, typeof(Sprite), false);
        if (targetAbility.type != previousType)
        {
            previousType = targetAbility.type;
            switch (targetAbility.type)
            {
                case AbilityType.Effect:
                    layout = () =>
                    {
#if Test
                        int i = 0;
                        targetAbility.effects = new List<ScriptableEffect>(this.SaveCollection("Effects", "", targetAbility.effects, true, ref showEffects, () =>
                         {
                             showDelays.Add(false);
                             targetAbility.delayedEffects.Add(false);
                             targetAbility.delayedEffectsDelays.Add(0);
                             return null;
                         },
                         x =>
                        {
                            ScriptableEffect result = null;
                            showDelays[i] = EditorGUILayout.Foldout(showDelays[i], "Effect", true);
                            result = (ScriptableEffect)EditorGUILayout.ObjectField(x, typeof(ScriptableEffect), false);
                            if (showDelays[i])
                            {
                                EditorGUILayout.BeginVertical();
                                EditorGUILayout.Space(15);
                                targetAbility.delayedEffects[i] = EditorGUILayout.Toggle(new GUIContent("Is Delayed", "Is the effect going to be delayed?"), targetAbility.delayedEffects[i]);
                                targetAbility.delayedEffectsDelays[i] = EditorGUILayout.FloatField(new GUIContent("Delay", "By how much the effect will be delayed"), targetAbility.delayedEffectsDelays[i]);
                                EditorGUILayout.EndVertical();
                            }
                            i++;
                            return result;

                        }));

#else
                        targetAbility.effects = new List<ScriptableEffect>(this.SaveCollection("Effects", "Effect", targetAbility.effects, ref showEffects, x => (ScriptableEffect)EditorGUILayout.ObjectField(x, typeof(ScriptableEffect), false)));
#endif
                    };
                    if (showDelays.Count > targetAbility.effects.Count)
                    {
                        for (int i = showDelays.Count - 1; i > targetAbility.effects.Count - 1; i--)
                        {
                            showDelays.RemoveAt(i);
                            targetAbility.delayedEffects.RemoveAt(i);
                            targetAbility.delayedEffectsDelays.RemoveAt(i);
                        }
                    }
                    break;
            }
        }
        layout();
        EditorGUILayout.EndVertical();
        if (EditorGUI.EndChangeCheck())
            EditorUtility.SetDirty(targetAbility);
    }

    private void RecalculateLayout()
    {
        //lessen the repetition
        //allow for on the spot effect creation?
    }
}
#endif