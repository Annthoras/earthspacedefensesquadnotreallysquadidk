﻿#define Test
#if UNITY_EDITOR
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Extensions.Unity.Editor;
[CustomEditor(typeof(ScriptableEffect))]
public class EffectEditor : Editor
{
    private ScriptableEffect targetEffect;
    bool showUniques;
    bool showCompartments;
    private void OnEnable()
    {
        targetEffect = (ScriptableEffect)this.target;
        showUniques = false;
        showCompartments = false;
    }
    public override void OnInspectorGUI()
    {
        EditorGUI.BeginChangeCheck();

        EditorGUILayout.BeginVertical();
        if (GUILayout.Button("Save To File"))
            FileManager.WriteToFile(targetEffect.name, new SerializableEffect(targetEffect),"eff");
        if (GUILayout.Button("Load From File"))
            targetEffect.FromSerialized(FileManager.ReadFromFile<SerializableEffect>(targetEffect.name+".eff"));
        targetEffect.ticks = EditorGUILayout.IntField(new GUIContent("Ticks", "The number of times the effect will activate over [duration] seconds"), targetEffect.ticks);
        if (targetEffect.ticks < 1)
            targetEffect.ticks = 1;
        targetEffect.duration = EditorGUILayout.FloatField(new GUIContent("Duration", "The time the effect will stay on the target in seconds"), targetEffect.duration);
        if (targetEffect.duration < 0)
            targetEffect.duration = 0;
        targetEffect.isOffensive = EditorGUILayout.Toggle(new GUIContent("Is Offensive", "Is it a debuff?"), targetEffect.isOffensive);
        targetEffect.isUnique = EditorGUILayout.Toggle(new GUIContent("Is Unique", "Can there only be one of this effect on the target?"), targetEffect.isUnique);

        targetEffect.uniques = new List<ScriptableEffect>(this.SaveCollection("Uniques", "Unique", targetEffect.uniques, ref showUniques, x => (ScriptableEffect)EditorGUILayout.ObjectField(x, typeof(ScriptableEffect), false)));
        targetEffect.graphic = (Sprite)EditorGUILayout.ObjectField(new GUIContent("Graphic", "The effect's icon"), targetEffect.graphic, typeof(Sprite), false);
        targetEffect.compartments = new List<EffectCompartment>(this.SaveCollection("Effects", "Effect", targetEffect.compartments, ref showCompartments, x =>
         {
             EditorGUILayout.BeginVertical();
             EffectCompartment result = x;
             result.affectedStat = (AffectableStatistic)EditorGUILayout.EnumPopup(new GUIContent("Affected Stat", "The stat that the effect will change"), result.affectedStat);
             result.calculation = (EffectCalculation)EditorGUILayout.EnumPopup(new GUIContent("Calculation", "How the strength of the effect will be determined"), result.calculation);
#if Test
             switch (result.calculation)
             {
                 case EffectCalculation.Value:
                     result.value = EditorGUILayout.FloatField(new GUIContent("Value", "The number by which to affect the given stat"), result.value);
                     break;
                 case EffectCalculation.PercentageOf:
                     result.targetUsedStat = (Statistic)EditorGUILayout.EnumPopup(new GUIContent("Used Stat", "The stat that [value] percent will be affecting [Affected Stat]"), result.targetUsedStat);
                     result.value = EditorGUILayout.Slider(new GUIContent("Percent", "How much percent of [Used Stat] to change [Affected Stat] by"), result.value, 0f, 1f);
                     break;
                 case EffectCalculation.PercentageOfByStat:
                 case EffectCalculation.MissingPercentageOf:
                     result.targetUsedStat = (Statistic)EditorGUILayout.EnumPopup(new GUIContent("Used Stat", "The stat that will be compared to [Comparison Stat] to determine the percentage"), result.targetUsedStat);
                     result.comparisonStat = (Statistic)EditorGUILayout.EnumPopup(new GUIContent("Comparison Stat", "The stat that [Used Stat] will be compared to to determine the percentage"), result.comparisonStat);
                     break;
             }
#else
             if (result.calculation != EffectCalculation.Value)
             {
                 result.targetUsedStat = (Statistic)EditorGUILayout.EnumPopup(new GUIContent("Used Stat", "The stat that will be divided by [Comparison Stat]"), result.targetUsedStat);
                 result.comparisonStat = (Statistic)EditorGUILayout.EnumPopup(new GUIContent("Comparison Stat", "The stat that [Used Stat] will be divided by"), result.comparisonStat);
             }
#endif
             result.isAdditive = EditorGUILayout.Toggle(new GUIContent("Is Additive", "Will the strength increase with each tick?"), result.isAdditive);
             EditorGUILayout.EndVertical();
             return result;
         }));

        EditorGUILayout.EndVertical();
        if (EditorGUI.EndChangeCheck())
            EditorUtility.SetDirty(targetEffect);
    }

}
#endif