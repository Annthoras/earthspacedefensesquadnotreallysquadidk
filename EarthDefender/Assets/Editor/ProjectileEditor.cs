﻿#define Test
#if UNITY_EDITOR
using System;
using System.Linq;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Extensions.Unity.Editor;
[CustomEditor(typeof(ScriptableProjectile))]
public class ProjectileEditor : Editor
{
    private ScriptableProjectile targetProjectile;
    private bool showEffects;
    private void OnEnable()
    {
        targetProjectile = (ScriptableProjectile)this.target;
        showEffects = false;
    }
    public override void OnInspectorGUI()
    {
        EditorGUI.BeginChangeCheck();

        EditorGUILayout.BeginVertical();
        targetProjectile.damage.ammount = EditorGUILayout.FloatField(new GUIContent("Damage Ammount", "The Ammount of damage the projectile will do"), targetProjectile.damage.ammount);
        targetProjectile.damage.type = (DamageType)EditorGUILayout.EnumPopup(new GUIContent("Damage Type", "The type of damage the projectile will have"), targetProjectile.damage.type);
        if (targetProjectile.damage.type == DamageType.Explosive)
            targetProjectile.explosionRadius = EditorGUILayout.FloatField(new GUIContent("Explosion Radius", "How far the explosion raches"), targetProjectile.explosionRadius);
        EditorGUILayout.Space();
        targetProjectile.flightSpeed = EditorGUILayout.FloatField(new GUIContent("Flight Speed", "How fast the projectile will travel"), targetProjectile.flightSpeed);
        EditorGUILayout.Space();
        targetProjectile.sprite = (Sprite)EditorGUILayout.ObjectField(new GUIContent("Sprite", "The appearance of the projectile"), targetProjectile.sprite, typeof(Sprite), false);
        targetProjectile.keepColor = EditorGUILayout.Toggle(new GUIContent("Keep Color", "Should the projectile have it's own color, or use that of the weapon?"), targetProjectile.keepColor);
        if (targetProjectile.keepColor)
            targetProjectile.color = EditorGUILayout.ColorField(new GUIContent("Color", "The color of the sprite"), targetProjectile.color);
        targetProjectile.extraEffects = new List<ScriptableEffect>(this.SaveCollection("Effects", "Effect", targetProjectile.extraEffects, ref showEffects, x => (ScriptableEffect)EditorGUILayout.ObjectField(x, typeof(ScriptableEffect), false)));

        EditorGUILayout.EndVertical();
        if (EditorGUI.EndChangeCheck())
            EditorUtility.SetDirty(targetProjectile);
    }
}
#endif