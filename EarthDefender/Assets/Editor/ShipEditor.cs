﻿#if UNITY_EDITOR
using System;
using System.Linq;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Extensions;
using Extensions.Unity.Editor;
public class ShipEditor : Editor
{
    protected ScriptableShip targetShip;
    private bool showAbilities;
    private bool showProjectiles;
    protected virtual void OnEnable()
    {
        targetShip = (ScriptableShip)this.target;
        showAbilities = false;
        showProjectiles = false;
        Resources.LoadAll<ScriptableProjectile>("Scriptables/Projectiles").ForEach(x => 
        {
            if (!targetShip.maxProjectiles.Contains(x))
            {
                targetShip.maxProjectiles.Add(x);
                targetShip.maxProjectilesAmmounts.Add(0);
            }
        });
    }
    protected void ShowSharedValues()
    {
        targetShip.baseHealth = EditorGUILayout.IntField(new GUIContent("Health", "The base health of the ship"), (int)targetShip.baseHealth);
        targetShip.baseShield = EditorGUILayout.IntField(new GUIContent("Shield", "The base shield of the ship"), (int)targetShip.baseShield);
        targetShip.baseArmor = EditorGUILayout.IntField(new GUIContent("Armor", "The base armor of the ship"), (int)targetShip.baseArmor);
        EditorGUILayout.Space();
        targetShip.baseShieldRegen = EditorGUILayout.FloatField(new GUIContent("Shield Regen", "The ammount of shield regenerated per second"), targetShip.baseShieldRegen);
        targetShip.baseShieldRegenDelay = EditorGUILayout.FloatField(new GUIContent("Shield Regen Delay", "How long it takes for the shield to start regenerating after taking damage (in seconds)"), targetShip.baseShieldRegenDelay);
        EditorGUILayout.Space();
        targetShip.baseEnergy = EditorGUILayout.IntField(new GUIContent("Energy", "The base energy of the ship"), (int)targetShip.baseEnergy);
        targetShip.baseSpeed = EditorGUILayout.FloatField(new GUIContent("Speed", "The base speed of the ship"), targetShip.baseSpeed);
        EditorGUILayout.Space();
        targetShip.baseFireRate = EditorGUILayout.FloatField(new GUIContent("Fire Rate", "The base fire rate of the ship"), targetShip.baseFireRate);
        targetShip.baseWeapon = (ScriptableWeapon)EditorGUILayout.ObjectField(new GUIContent("Base Weapon", "The basic weapon of the ship"), targetShip.baseWeapon, typeof(ScriptableWeapon), false);
        targetShip.turretTurnSpeed = EditorGUILayout.FloatField(new GUIContent("Turn Speed", "How fast the weapon turns"), targetShip.turretTurnSpeed);
        EditorGUILayout.Space();
        targetShip.sprite = (Sprite)EditorGUILayout.ObjectField(new GUIContent("Sprite", "The appearance of the ship"), targetShip.sprite, typeof(Sprite), false);
        targetShip.color = EditorGUILayout.ColorField(new GUIContent("Color", "The ship's color"), targetShip.color);
        targetShip.abilities = new List<ScriptableAbilitiy>(this.SaveCollection("Abilities", "Ability", targetShip.abilities, ref showAbilities, x => (ScriptableAbilitiy)EditorGUILayout.ObjectField(x, typeof(ScriptableAbilitiy), false)));
        int i = -1;
        targetShip.maxProjectilesAmmounts = new List<int>(this.SaveCollection("Max Ammo", "", targetShip.maxProjectilesAmmounts, false, ref showProjectiles, x => 
        {
            i++;
            EditorGUILayout.LabelField(targetShip.maxProjectiles[i].name);
            return EditorGUILayout.IntField(x);
        }));
    }
}
#endif