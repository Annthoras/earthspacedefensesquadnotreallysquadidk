﻿#define Test
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;
using Extensions.Unity.Editor;
[CustomEditor(typeof(ScriptableDefender))]
public class DefenderEditor : ShipEditor
{
    protected ScriptableDefender targetDefender;
    private bool showWeapons;
    protected override void OnEnable()
    {
        base.OnEnable();
        targetDefender = (ScriptableDefender)targetShip;
        showWeapons = false;
    }
    public override void OnInspectorGUI()
    {
        EditorGUI.BeginChangeCheck();
        EditorGUILayout.BeginVertical();
        ShowSharedValues();
        targetDefender.weapons = new List<ScriptableWeapon>(this.SaveCollection("Weapons", "Weapon", targetDefender.weapons, ref showWeapons, x => (ScriptableWeapon)EditorGUILayout.ObjectField(x, typeof(ScriptableWeapon), false)));
        EditorGUILayout.EndVertical();
        if (EditorGUI.EndChangeCheck())
            EditorUtility.SetDirty(targetShip);
    }
}

