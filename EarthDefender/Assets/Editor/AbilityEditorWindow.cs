﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using UnityEngine;
//using UnityEditor;

//public class AbilityEditorWindow : EditorWindow
//{
//    static ScriptableAbilitiy target;
//    static Vector2 scrollPosition;
//    static Action layout;
//    static bool showEffects;
//    static bool wasNull;
//    static AbilityEditorWindow window;
//    static AbilityType previousType;
//    [MenuItem("Window/AbilityEditor")]
//    static void Init()
//    {
//        window = (AbilityEditorWindow)GetWindow(typeof(AbilityEditorWindow));
//        window.Show();
//        wasNull = true;
//        previousType = AbilityType.None;
//    }
//    private void OnGUI()
//    {
//        EditorGUILayout.BeginVertical();
//        scrollPosition = EditorGUILayout.BeginScrollView(scrollPosition, GUILayout.ExpandHeight(true));
//        target = (ScriptableAbilitiy)EditorGUILayout.ObjectField("Ability", target, typeof(ScriptableAbilitiy), false);
//        if (target != null)
//        {
//            if (wasNull)
//            {
//                RecalculateLayout();
//                wasNull = false;
//                previousType = target.type;
//            }
//            if (target.type != previousType)
//            {
//                RecalculateLayout();
//                previousType = target.type;
//            }
//            target.type = (AbilityType)EditorGUILayout.EnumPopup(new GUIContent("Type", "The ability's type"), target.type);
//            target.energyCost = EditorGUILayout.IntField(new GUIContent("Cost", "The amount of energy it will cost to use the ability"), target.energyCost);
//            target.sprite = (Sprite)EditorGUILayout.ObjectField("Sprite", target.sprite, typeof(Sprite), false);
//            layout();
//        }
//        else
//        {
//            wasNull = true;
//            previousType = AbilityType.None;
//        }
//        EditorGUILayout.EndScrollView();
//        EditorGUILayout.EndVertical();
//    }
//    private void RecalculateLayout()
//    {
//    //lessen the repetition
//    //allow for on the spot effect creation?
//        switch (target.type)
//        {
//            case AbilityType.Bubble:
//                layout = () =>
//                {
//                    target.radius = EditorGUILayout.FloatField(new GUIContent("Radius", "The bubble's radius"), target.radius);
//                    target.duration = EditorGUILayout.FloatField(new GUIContent("Duration", "The bubble's duration"), target.duration);
//                    target.prefab = (GameObject)EditorGUILayout.ObjectField("Prefab", target.prefab, typeof(GameObject), false);
//                    int l = target.effects.Length;
//                    EditorGUILayout.BeginHorizontal();
//                    showEffects = EditorGUILayout.Foldout(showEffects, "Effects", true);
//                    GUILayout.Space(20);
//                    l = EditorGUILayout.IntField("Length", l);
//                    EditorGUILayout.EndHorizontal();
//                    if (l > target.effects.Length)
//                    {
//                        List<Tuple<ScriptableEffect, bool>> effects = new List<Tuple<ScriptableEffect, bool>>(target.effects);
//                        for (int i = target.effects.Length; i < l; i++)
//                            effects.Add(new Tuple<ScriptableEffect, bool>(null, false));
//                        target.effects = effects.ToArray();
//                    }
//                    else if (l < target.effects.Length)
//                    {
//                        List<Tuple<ScriptableEffect, bool>> effects = new List<Tuple<ScriptableEffect, bool>>();
//                        for (int i = 0; i < l; i++)
//                            effects.Add(target.effects[i]);
//                        target.effects = effects.ToArray();
//                    }
//                    if (showEffects)
//                    {
//                        for (int i = 0; i < l; i++)
//                        {
//                            EditorGUILayout.BeginHorizontal();
//                            GUILayout.Space(50);
//                            ScriptableEffect effect = target.effects[i].Item1;
//                            EditorGUILayout.LabelField("Effect " + i);
//                            effect = (ScriptableEffect)EditorGUILayout.ObjectField(effect, typeof(ScriptableEffect), false);
//                            bool useOnSelf = target.effects[i].Item2;
//                            GUILayout.Space(20);
//                            useOnSelf = EditorGUILayout.Toggle(new GUIContent("Use on self", "Should the effect be applied on the user?"), useOnSelf);
//                            EditorGUILayout.EndHorizontal();
//                            Debug.Log(target.effects[i].Item1.name);
//                            target.effects[i] = new Tuple<ScriptableEffect, bool>(effect, useOnSelf);
//                            Debug.Log(target.effects[i].Item1.name);
//                        }

//                    }
//                };
//                break;
//            case AbilityType.Shockwave:
//                layout = () =>
//                {
//                    target.radius = EditorGUILayout.FloatField(new GUIContent("Radius", "The shockwave's end radius"), target.radius);
//                    target.duration = EditorGUILayout.FloatField(new GUIContent("Duration", "The ammount of time it takes the shockwave to get to it's end radius"), target.duration);
//                    target.prefab = (GameObject)EditorGUILayout.ObjectField("Prefab", target.prefab, typeof(GameObject), false);
//                    int l = target.effects.Length;
//                    EditorGUILayout.BeginHorizontal();
//                    showEffects = EditorGUILayout.Foldout(showEffects, "Effects", true);
//                    GUILayout.Space(20);
//                    l = EditorGUILayout.IntField("Length", l);
//                    EditorGUILayout.EndHorizontal();
//                    if (l > target.effects.Length)
//                    {
//                        List<Tuple<ScriptableEffect, bool>> effects = new List<Tuple<ScriptableEffect, bool>>(target.effects);
//                        for (int i = target.effects.Length; i < l; i++)
//                            effects.Add(new Tuple<ScriptableEffect, bool>(null, false));
//                        target.effects = effects.ToArray();
//                    }
//                    else if (l < target.effects.Length)
//                    {
//                        List<Tuple<ScriptableEffect, bool>> effects = new List<Tuple<ScriptableEffect, bool>>();
//                        for (int i = 0; i < l; i++)
//                            effects.Add(target.effects[i]);
//                        target.effects = effects.ToArray();
//                    }
//                    if (showEffects)
//                    {
//                        for (int i = 0; i < l; i++)
//                        {
//                            EditorGUILayout.BeginHorizontal();
//                            GUILayout.Space(50);
//                            ScriptableEffect effect = target.effects[i].Item1;
//                            EditorGUILayout.LabelField("Effect " + i);
//                            effect = (ScriptableEffect)EditorGUILayout.ObjectField(effect, typeof(ScriptableEffect), false);
//                            bool useOnSelf = target.effects[i].Item2;
//                            GUILayout.Space(20);
//                            useOnSelf = EditorGUILayout.Toggle(new GUIContent("Use on self", "Should the effect be applied on the user?"), useOnSelf);
//                            EditorGUILayout.EndHorizontal();
//                            target.effects[i] = new Tuple<ScriptableEffect, bool>(effect, useOnSelf);
//                        }
//                    }
//                };
//                break;
//            case AbilityType.ControllableWeapon:
//            case AbilityType.UncontrollableWeapon:
//            case AbilityType.AreaOfEffect:
//            case AbilityType.Movement:
//            case AbilityType.None:
//                layout = () => { };
//                break;
//            case AbilityType.Effect:
//                layout = () =>
//                {
//                    EditorGUILayout.BeginHorizontal();
//                    showEffects = EditorGUILayout.Foldout(showEffects, "Effects", true);
//                    GUILayout.Space(20);
//                    if (target.effects == null)
//                        target.effects = new Tuple<ScriptableEffect, bool>[] { };
//                    int length = target.effects.Length;
//                    length = EditorGUILayout.IntField("Length", length);
//                    EditorGUILayout.EndHorizontal();
//                    if (length > target.effects.Length)
//                    {
//                        List<Tuple<ScriptableEffect, bool>> effects = new List<Tuple<ScriptableEffect, bool>>(target.effects);
//                        for (int i = target.effects.Length; i < length; i++)
//                            effects.Add(new Tuple<ScriptableEffect, bool>(null, false));
//                        target.effects = effects.ToArray();
//                    }
//                    else if (length < target.effects.Length)
//                    {
//                        List<Tuple<ScriptableEffect, bool>> effects = new List<Tuple<ScriptableEffect, bool>>();
//                        for (int i = 0; i < length; i++)
//                            effects.Add(target.effects[i]);
//                        target.effects = effects.ToArray();
//                    }
//                    if (showEffects)
//                    {
//                        for (int i = 0; i < length; i++)
//                        {
//                            EditorGUILayout.BeginHorizontal();
//                            GUILayout.Space(50);
//                            ScriptableEffect effect = target.effects[i].Item1;
//                            EditorGUILayout.LabelField("Effect " + i);
//                            effect = (ScriptableEffect)EditorGUILayout.ObjectField(effect, typeof(ScriptableEffect), false);
//                            bool useOnSelf = target.effects[i].Item2;
//                            GUILayout.Space(20);
//                            useOnSelf = EditorGUILayout.Toggle(new GUIContent("Use on self", "Should the effect be applied on the user?"), useOnSelf);
//                            EditorGUILayout.EndHorizontal();
//                            target.effects[i] = new Tuple<ScriptableEffect, bool>(effect, useOnSelf);
//                        }
//                    }

//                };
//                break;
//            case AbilityType.DelayedEffect:
//                layout = () =>
//                {
//                    target.duration = EditorGUILayout.FloatField(new GUIContent("Delay duration", "The time between each effect batch"), target.duration);
//                    EditorGUILayout.BeginHorizontal();
//                    showEffects = EditorGUILayout.Foldout(showEffects, "Effect batches", true);
//                    GUILayout.Space(50);
//                    target.delays = EditorGUILayout.IntField("Batches", target.delays);
//                    EditorGUILayout.EndHorizontal();
//                    if (target.delays > target.delayedEffects.Length)
//                    {
//                        List<Tuple<ScriptableEffect, bool>[]> delays = new List<Tuple<ScriptableEffect, bool>[]>(target.delayedEffects);
//                        List<bool> states = new List<bool>(target.foldoutStates);
//                        for (int i = target.delayedEffects.Length; i < target.delays; i++)
//                        {
//                            delays.Add(new Tuple<ScriptableEffect, bool>[] { });
//                            states.Add(false);
//                        }
//                        target.delayedEffects = delays.ToArray();
//                        target.foldoutStates = states.ToArray();
//                    }
//                    else if (target.delays < target.delayedEffects.Length)
//                    {
//                        List<Tuple<ScriptableEffect, bool>[]> delays = new List<Tuple<ScriptableEffect, bool>[]>();
//                        List<bool> states = new List<bool>();
//                        for (int i = 0; i < target.delays; i++)
//                        {
//                            delays.Add(target.delayedEffects[i]);
//                            states.Add(target.foldoutStates[i]);
//                        }
//                        target.delayedEffects = delays.ToArray();
//                        target.foldoutStates = states.ToArray();
//                    }
//                    if (showEffects)
//                    {
//                        for (int i = 0; i < target.delays; i++)
//                        {
//                            EditorGUILayout.BeginHorizontal();
//                            GUILayout.Space(50);
//                            target.foldoutStates[i] = EditorGUILayout.Foldout(target.foldoutStates[i], "Batch " + i, true);
//                            GUILayout.Space(20);
//                            int length = target.delayedEffects[i].Length;
//                            length = EditorGUILayout.IntField("Length", length);
//                            EditorGUILayout.EndHorizontal();
//                            if (length > target.delayedEffects[i].Length)
//                            {
//                                List<Tuple<ScriptableEffect, bool>> batch = new List<Tuple<ScriptableEffect, bool>>(target.delayedEffects[i]);
//                                for (int l = target.delayedEffects[i].Length; l < length; l++)
//                                    batch.Add(new Tuple<ScriptableEffect, bool>(null, false));
//                                target.delayedEffects[i] = batch.ToArray();
//                            }
//                            else if (length < target.delayedEffects.Length)
//                            {
//                                List<Tuple<ScriptableEffect, bool>> batch = new List<Tuple<ScriptableEffect, bool>>();
//                                for (int l = 0; l < length; l++)
//                                    batch.Add(target.delayedEffects[i][l]);
//                                target.delayedEffects[i] = batch.ToArray();
//                            }
//                            if (target.foldoutStates[i])
//                            {
//                                for (int l = 0; l < length; l++)
//                                {
//                                    EditorGUILayout.BeginHorizontal();
//                                    GUILayout.Space(90);
//                                    ScriptableEffect effect = target.delayedEffects[i][l].Item1;
//                                    bool useOnSelf = target.delayedEffects[i][l].Item2;
//                                    effect = (ScriptableEffect)EditorGUILayout.ObjectField("Effect " + l, effect, typeof(ScriptableEffect), false);
//                                    GUILayout.Space(20);
//                                    useOnSelf = EditorGUILayout.Toggle("Use on self", useOnSelf);
//                                    EditorGUILayout.EndHorizontal();
//                                }
//                            }
//                        }
//                    }
//                };
//                break;

//            default:
//                break;
//        }
//    }
//}
